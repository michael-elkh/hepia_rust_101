// ANCHOR: io_module
//! Contains functions to interact with the user, either
//! by reading inputs from the terminal, either by writing values
//! in it.
//ANCHOR_END: io_module

// ANCHOR: function
/// Poorly emulates the parsing of a command line.
pub fn read_command_line() -> [i32; crate::SIZE] {
    [10, 32, 12, 43, 52, 53, 83, 2, 9]
}
// ANCHOR_END: function

/// Prints all the elements of the `tab`.
/// Tab is borrowed here
pub fn print_tab(tab: &[i32; crate::SIZE]) {
    for t in tab {
        print!("{} ", t);
    }
    println!();
}
