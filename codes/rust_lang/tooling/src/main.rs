use tooling::io;
use tooling::something_or_nothing::find_min;

fn main() {
    let tab = io::read_command_line();
    println!("Among the Somethings in the list:");
    io::print_tab(&tab);
    let min = find_min(&tab);
    min.print();
}
