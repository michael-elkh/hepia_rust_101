use std::cmp::Ordering;

use crate::minimum::Minimum;

/// Larger ints based on a [Vec] of [u8] to repensent arbitrary lengthy numbers.
/// The number has a sign as well.
// ANCHOR: custom_int
#[derive(Debug)]
pub struct CustomInt<'a> {
    /// The data contains the unsigned integers that are read from right to left
    /// The number 1337 is stored as vec![7, 3, 3, 1]. Each number must be in the range [0,9]
    /// and no trailing 0s are allowed.
    data: &'a Vec<u8>,
    /// Contains the sign of the number +/-1;
    sign: i8,
}
// ANCHOR_END: custom_int

// ANCHOR: custom_int_impl
impl<'a> CustomInt<'a>
// ANCHOR_END: custom_int_impl
{
    /// Tries to create a new [CustomInt]. If the number is valid it returns
    /// an Ok(CustomInt) an Error otherwise.
    ///
    /// # Examples
    ///
    /// ```
    /// use lifetimes::custom_int::CustomInt;
    /// let v1 = vec![1, 2, 3, 4];
    /// let num = CustomInt::try_new(&v1, 1);
    /// assert!(num.is_ok());
    /// let num = CustomInt::try_new(&v1, -1);
    /// assert!(num.is_ok());
    /// let num = CustomInt::try_new(&v1, 10);
    /// assert!(num.is_err());
    /// let num = CustomInt::try_new(&v1, -10);
    /// assert!(num.is_err());
    /// let v1 = vec![];
    /// let num = CustomInt::try_new(&v1, -1);
    /// assert!(num.is_err());
    /// ```
    ///
    // ANCHOR: try_new
    pub fn try_new(data: &'a Vec<u8>, sign: i8) -> Result<Self, String> {
        if data.is_empty() {
            Err(String::from("Data is empty."))
        } else if sign == 1 || sign == -1 {
            Ok(CustomInt { data, sign })
        } else {
            Err(String::from("Invalid sign."))
        }
    }
    // ANCHOR_END: try_new
}

// ANCHOR: minimum
impl<'a> Minimum<'a> for CustomInt<'a>
// ANCHOR_END: minimum
{
    // ANCHOR: min
    fn min(&'a self, rhs: &'a Self) -> &'a Self {
        match self.sign.cmp(&rhs.sign) {
            Ordering::Less => return self,
            Ordering::Greater => return rhs,
            Ordering::Equal => match self.data.len().cmp(&rhs.data.len()) {
                Ordering::Less => {
                    if self.sign == 1 {
                        return self;
                    } else {
                        return rhs;
                    }
                }
                Ordering::Greater => {
                    if self.sign == 1 {
                        return rhs;
                    } else {
                        return self;
                    }
                }
                Ordering::Equal => {
                    for (l, r) in self.data.iter().rev().zip(rhs.data.iter().rev()) {
                        let ls = (*l as i8) * self.sign;
                        let rs = (*r as i8) * self.sign;
                        match ls.cmp(&rs) {
                            Ordering::Less => return self,
                            Ordering::Greater => return rhs,
                            Ordering::Equal => {}
                        }
                    }
                }
            },
        }
        self
    }
    // ANCHOR_END: min
}

// ANCHOR: partialeq
impl<'a> PartialEq for CustomInt<'a>
// ANCHOR_END: partialeq
{
    fn eq(&self, other: &Self) -> bool {
        if self.sign == other.sign && self.data.len() == other.data.len() {
            self.data
                .iter()
                .zip(other.data.iter())
                .try_fold(true, |_, (l, r)| if *l == *r { Some(true) } else { None })
                .is_some()
        } else {
            false
        }
    }
}

// ANCHOR: display
impl<'a> std::fmt::Display for CustomInt<'a>
// ANCHOR_END: display
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // This could be replaced by an `?`
        if self.sign == -1 {
            write!(f, "-")?;
        }

        // This could be replaced by an `?`
        let res = self
            .data
            .iter()
            .rev()
            .try_fold((), |_, t| write!(f, "{}", t));
        res
    }
}

#[cfg(test)]
mod tests {
    use crate::custom_int::CustomInt;
    use crate::minimum::Minimum;
    use crate::something_or_nothing::find_min;

    #[test]
    fn test_creation() {
        let v1 = vec![1, 2, 3, 4];
        CustomInt::try_new(&v1, 1).unwrap();
        CustomInt::try_new(&v1, -1).unwrap();
    }

    #[test]
    #[should_panic]
    fn test_failure_creation_sign() {
        let v1 = vec![1, 2, 3, 4];
        CustomInt::try_new(&v1, 10).unwrap();
    }

    #[test]
    #[should_panic]
    fn test_failure_creation_sign2() {
        let v1 = vec![1, 2, 3, 4];
        CustomInt::try_new(&v1, 0).unwrap();
    }

    #[test]
    #[should_panic]
    fn test_failure_creation_data() {
        let v1 = vec![];
        CustomInt::try_new(&v1, 1).unwrap();
    }

    #[test]
    fn test_min() {
        let mut v = Vec::new();
        let v1 = vec![1, 2, 3, 4];
        let v2 = vec![1, 2, 3];
        let lhs = CustomInt::try_new(&v1, 1).unwrap();
        let rhs = CustomInt::try_new(&v2, 1).unwrap();
        assert!(rhs == *lhs.min(&rhs));
        v.push(lhs);
        v.push(rhs);
        let lhs = CustomInt::try_new(&v1, -1).unwrap();
        let rhs = CustomInt::try_new(&v2, -1).unwrap();
        assert!(lhs == *lhs.min(&rhs));
        v.push(lhs);
        v.push(rhs);
        let v1 = vec![1, 2, 3, 4];
        let v2 = vec![1, 2, 5, 4];
        let lhs = CustomInt::try_new(&v1, -1).unwrap();
        let rhs = CustomInt::try_new(&v2, -1).unwrap();
        assert!(rhs == *lhs.min(&rhs));
        v.push(lhs);
        v.push(rhs);
        let lhs = CustomInt::try_new(&v1, 1).unwrap();
        let rhs = CustomInt::try_new(&v2, 1).unwrap();
        assert!(lhs == *lhs.min(&rhs));
        let min = find_min(&v);
        assert_eq!(*min.unwrap(), CustomInt::try_new(&v2, -1).unwrap());
    }
}
