use crate::custom_int::CustomInt;

/// Prints all the elements of the `tab`.
/// Tab is borrowed here
pub fn print_tab(tab: &Vec<i32>) {
    for t in tab {
        print!("{} ", t);
    }
    println!();
}

/// Prints all the elements of the `tab`.
/// Tab is borrowed here
pub fn print_tab_custom_int(tab: &Vec<CustomInt>) {
    for i in tab {
        println!("{i} ");
    }
    println!();
}
