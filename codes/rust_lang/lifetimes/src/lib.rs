/*!
lifetimes illustrates the use of [Vec] and the Error Handling with [Option] and [Result].
It also showcases struct enums.
*/

pub mod custom_int;
pub mod io;
mod minimum;
pub mod something_or_nothing;

#[cfg(test)]
mod tests {
    use crate::minimum::Minimum;
    use crate::something_or_nothing::{find_min, SomethingOrNothing};

    #[test]
    fn test_creation() {
        let n1: SomethingOrNothing<i32> = SomethingOrNothing::default();
        assert!(n1 == SomethingOrNothing::default());
        let n2: SomethingOrNothing<i32> = SomethingOrNothing::new(1);
        assert!(n2 == SomethingOrNothing::new(1));
    }

    #[test]
    #[should_panic]
    fn test_failure_creation() {
        let n2: SomethingOrNothing<i32> = SomethingOrNothing::new(1);
        assert!(n2 == SomethingOrNothing::default());
        assert!(n2 == SomethingOrNothing::new(2));
    }

    #[test]
    fn test_min() {
        let a = vec![1, 5, -1, 2, 0, 10, 11, 0, 3];
        let min = find_min(&a);
        assert!(*min.unwrap() == -1);
    }

    #[test]
    fn test_min_empty() {
        let a: Vec<i32> = vec![];
        let min = find_min(&a);
        assert!(min == SomethingOrNothing::default());
    }

    #[test]
    fn test_min_i32() {
        let x = 5;
        let y = 10;
        assert_eq!(*Minimum::min(&x, &y), x);
        assert_eq!(*Minimum::min(&y, &x), x);
        assert_eq!(*Minimum::min(&x, &x), x);
        assert_eq!(*Minimum::min(&y, &y), y);
    }

    #[test]
    fn test_min_something_or_nothing() {
        let x = SomethingOrNothing::new(5i32);
        let y = SomethingOrNothing::new(10i32);
        let z = SomethingOrNothing::default();
        assert!(*x.min(&y) == x);
        assert!(*y.min(&x) == x);
        assert!(*z.min(&y) == y);
        assert!(*y.min(&z) == y);
        assert!(*z.min(&z) == z);
    }
}
