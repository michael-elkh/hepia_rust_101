use lifetimes::custom_int::CustomInt;
use lifetimes::io;
use lifetimes::something_or_nothing::find_min;

// ANCHOR: main
fn main() -> Result<(), String> {
    let v1 = vec![1, 3, 6, 9];
    let v2 = vec![2, 4, 2, 1];
    let v3 = vec![7, 4, 5, 3];
    let v4 = vec![4, 1, 1, 1];
    let v5 = vec![2, 5, 1, 8];
    let v6 = vec![5, 1, 5, 2];
    let v7 = vec![7, 6, 6, 7];
    let v8 = vec![8, 2, 2, 2];
    let lhs = vec![
        CustomInt::try_new(&v1, 1)?,
        CustomInt::try_new(&v2, -1)?,
        CustomInt::try_new(&v3, 1)?,
        CustomInt::try_new(&v4, -1)?,
        CustomInt::try_new(&v5, 1)?,
        CustomInt::try_new(&v6, 1)?,
        CustomInt::try_new(&v7, 1)?,
        CustomInt::try_new(&v8, 1)?,
    ];

    println!("Among the custom ints in the list:");
    io::print_tab_custom_int(&lhs);
    let min = find_min(&lhs);
    println!("The minimum is {min}");
    Ok(())
}
// ANCHOR_END: main
