// ANCHOR: minimum
pub trait Minimum<'a> {
    fn min(&'a self, rhs: &'a Self) -> &'a Self;
}
// ANCHOR_END: minimum

// ANCHOR: min
impl<'a> Minimum<'a> for i32 {
    fn min(&'a self, rhs: &'a Self) -> &'a Self {
        if self < rhs {
            self
        } else {
            rhs
        }
    }
}
// ANCHOR_END: min
