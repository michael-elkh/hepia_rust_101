use collections::io;
use collections::something_or_nothing::find_min;

fn main() -> Result<(), String> {
    //ANCHOR: vec
    let tab: Vec<i32> = io::read_command_line(10usize);
    println!("Among the Somethings in the list:");
    io::print_tab(&tab);
    let min = find_min(&tab);
    min.print();
    //ANCHOR_END: vec

    let tab = io::read_command_line_str()?;
    println!("Among the Somethings in the list:");
    io::print_tab(&tab);
    let min = find_min(&tab);
    min.print();

    //ANCHOR: ref
    println!("Among the Somethings in the list:");
    io::print_tab(&tab[1..9]);
    let min = find_min(&tab[1..9]);
    min.print();
    //ANCHOR_END: ref

    //ANCHOR: tab
    let tab = [1, 2, 3, 4, 5, 6];
    println!("Among the Somethings in the list:");
    io::print_tab(&tab);
    let min = find_min(&tab);
    min.print();
    //ANCHOR_END: tab

    Ok(())
}
