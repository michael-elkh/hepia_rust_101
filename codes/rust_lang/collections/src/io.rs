//! Contains functions to interact with the user, either
//! by reading inputs from the terminal, either by writing values
//! in it.
use rand::Rng;

// ANCHOR: read_command_line
/// Poorly emulates the parsing of a command line.
pub fn read_command_line(len: usize) -> Vec<i32> {
    let mut rng = rand::thread_rng();
    // ANCHOR: vec_new
    let mut v: Vec<i32> = Vec::new();
    // ANCHOR_END: vec_new
    // ANCHOR: vec_for
    for _i in 0..len {
        // ANCHOR: vec_push
        v.push(rng.gen());
        // ANCHOR: vec_push
    }
    // ANCHOR_END: vec_for
    v
}
// ANCHOR_END: read_command_line

// ANCHOR: read_command_line_str
/// Poorly emulates the parsing of a command line.
pub fn read_command_line_str() -> Result<Vec<i32>, String> {
    // ANCHOR: from
    let mut s = String::from("20 10 48 58 29 0 58 -10 39 5485 394");
    // ANCHOR_END: from
    // ANCHOR: push_str
    s.push_str(" -100");
    // ANCHOR_END: push_str
    // ANCHOR: push_char
    s.push(' ');
    s.push('1');
    s.push('2');
    // ANCHOR_END: push_char
    // ANCHOR: split
    let s: Vec<&str> = s.split_ascii_whitespace().collect();
    // ANCHOR_END: split

    // ANCHOR: string_for
    let mut v = Vec::new();
    for i in 0..s.len() {
        v.push(
            // ANCHOR: conversion
            s.get(i)
                .ok_or(String::from("Unable to index"))?
                .parse()
                .map_err(|_| format!("Unable to parse {}", s[i]))?,
            // ANCHOR_END: conversion
        );
    }
    // ANCHOR_END: string_for
    Ok(v)
}
// ANCHOR_END: read_command_line_str

/// Prints all the elements of the `tab`.
/// Tab is borrowed here
// ANCHOR: print_tab
pub fn print_tab(tab: &[i32]) {
    for t in tab {
        print!("{} ", t);
    }
    println!();
}
// ANCHOR_END: print_tab
