// ANCHOR: main_imports
use modules_visibilite::io;
use modules_visibilite::something_or_nothing::find_min;
// ANCHOR_END: main_imports

fn main() {
    // modules_visibilite::io is imported but not read_command_line
    let tab = io::read_command_line();
    println!("Among the Somethings in the list:");
    // modules_visibilite::io is imported but not print_tab
    io::print_tab(&tab);
    // modules_visibilite::something_or_nothing::find_min is imported and can be used directly
    let min = find_min(&tab);
    min.print();
}
