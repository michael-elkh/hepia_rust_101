// ANCHOR: trait
pub trait Minimum: Copy {
    fn min(self, rhs: Self) -> Self;
}
// ANCHOR_END: trait

impl Minimum for i32 {
    fn min(self, rhs: Self) -> Self {
        if self < rhs {
            self
        } else {
            rhs
        }
    }
}
