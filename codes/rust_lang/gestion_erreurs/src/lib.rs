//! This crate shows us different ways of dealing with errors in a Rust program.
//! You will find examples of [Option], [Result] and [panic!].

pub mod find_minimum;
pub mod io;
pub mod minimum;

#[cfg(test)]
mod tests {
    use crate::find_minimum::{
        find_min_amongst_arrays_by_hand, find_min_amongst_arrays_qm_op, find_min_with_option,
        find_min_with_panic, find_min_with_result,
    };
    const TAB: [i32; 9] = [10, 32, 12, 43, 52, 53, 83, 2, 9];
    const TAB_B: [i32; 9] = [22, 34, 11, 4, 52, 99, 71, 13, 43];
    const TAB_EMPTY: [i32; 0] = [];
    const MIN_TAB: i32 = 2;

    #[test]
    fn test_find_min_option() {
        let min = find_min_with_option(&TAB);

        assert!(min == Some(MIN_TAB));
    }

    #[test]
    fn test_find_min_option_empty() {
        let min = find_min_with_option(&TAB_EMPTY);

        assert!(min.is_none());
    }

    #[test]
    fn test_find_min_result() {
        let min = find_min_with_result(&TAB);

        assert!(min == Ok(MIN_TAB));
    }

    #[test]
    fn test_find_min_result_empty() {
        let min = find_min_with_result(&TAB_EMPTY);

        assert!(min.is_err());
    }

    #[test]
    fn test_find_min_panic() {
        let min = find_min_with_panic(&TAB);

        assert!(min == MIN_TAB);
    }

    #[test]
    #[should_panic]
    fn test_find_min_panic_empty() {
        let _min = find_min_with_panic(&TAB_EMPTY);
    }

    #[test]
    fn test_find_min_amongst_arrays_bh() {
        let min = find_min_amongst_arrays_by_hand(&TAB, &TAB_B);

        assert!(min == Ok(MIN_TAB));
    }

    #[test]
    fn test_find_min_amongst_arrays_qm() {
        let min = find_min_amongst_arrays_qm_op(&TAB, &TAB_B);

        assert!(min == Ok(MIN_TAB));
    }

    #[test]
    fn test_find_min_amongst_arrays_bh_empty() {
        let min = find_min_amongst_arrays_by_hand(&TAB, &TAB_EMPTY);

        assert!(min.is_err());
    }

    #[test]
    fn test_find_min_amongst_arrays_qm_empty() {
        let min = find_min_amongst_arrays_qm_op(&TAB, &TAB_EMPTY);

        assert!(min.is_err());
    }
}
