//! Contains functions to interact with the user, either
//! by reading inputs from the terminal, either by writing values
//! in it.

/// Poorly emulates the parsing of a command line.
pub fn read_command_line_correct() -> [i32; 9] {
    [10, 32, 12, 43, 52, 53, 83, 2, 9]
}

/// Poorly emulates the parsing of a command line.
pub fn read_empty_command_line() -> [i32; 0] {
    []
}

/// Prints all the elements of the `tab`.
/// Tab is borrowed here
pub fn print_tab(tab: &[i32]) {
    print!("[ ");
    for t in tab {
        print!("{} ", t);
    }
    println!("]");
}
