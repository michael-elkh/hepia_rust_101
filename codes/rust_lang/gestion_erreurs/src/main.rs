use gestion_erreurs::find_minimum::{
    find_min_amongst_arrays_qm_op, find_min_with_option, find_min_with_result,
    FindMinError::EmptyList, FindMinError::UnsupportedError,
};
use gestion_erreurs::io;

fn main() {
    let tab = io::read_command_line_correct();
    println!("Among the elements in the list:");
    io::print_tab(&tab);
    let min = find_min_with_option(&tab);
    match min {
        Some(val) => print!("The minimum value is {}", val),
        None => eprintln!("There is no minimum since the list is empty"),
    }
    println!("");
    println!("");

    let tab_empty = io::read_empty_command_line();
    println!("Among the elements in the list:");
    io::print_tab(&tab_empty);

    //ANCHOR: parse_result
    let min = find_min_with_result(&tab_empty);
    match min {
        Ok(val) => print!("The minimum value is {}", val),
        Err(EmptyList) => eprintln!("The array is empty"),
        Err(UnsupportedError(msg)) => panic!("Unsupported error : {}", msg),
    }
    //ANCHOR_END: parse_result

    println!("Among the elements in the lists:");
    io::print_tab(&tab);
    io::print_tab(&tab_empty);
    let min = find_min_amongst_arrays_qm_op(&tab, &tab_empty);
    match min {
        Ok(val) => print!("The minimum value is {}", val),
        Err(EmptyList) => eprintln!("One or both arrays are empty"),
        Err(UnsupportedError(msg)) => panic!("Unsupported error : {}", msg),
    }
}
