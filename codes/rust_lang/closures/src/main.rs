use closures::binary_operator::{minimum_operator, sum_operator};
use closures::find::find_with_hof;
use closures::io;

fn main() {
    let tab = io::read_command_line_correct();
    println!("Among the elements in the list:");
    io::print_tab(&tab);

    //ANCHOR: min_usage
    let min = find_with_hof(&tab, minimum_operator());
    match min {
        Some(val) => println!("The minimum value is {}", val),
        None => eprintln!("There is no minimum"),
    }
    //ANCHOR_END: min_usage

    //ANCHOR: max_variable
    let max_op: fn(i32, i32) -> i32 = |x, y| if x >= y { x } else { y };
    //ANCHOR_END: max_variable

    //ANCHOR: option_filter
    let max_val: Option<i32> = find_with_hof(&tab, max_op);
    let odd_max: Option<i32> = max_val.filter(|x| x % 2 == 1);
    match odd_max {
        Some(_) => println!("The maximum value is an odd number"),
        None => {
            if max_val.is_some() {
                println!("The maximum value is an even number")
            } else {
                eprintln!("There is no maximum")
            }
        }
    }
    //ANCHOR_END: option_filter

    //ANCHOR: option_map
    let two: f32 = 2.0f32;

    let sum: Option<i32> = find_with_hof(&tab, sum_operator());
    let half: Option<f32> = sum.map(|x: i32| (x as f32) / two);
    match half {
        Some(val) => println!("The sum of the elements divided by two is {}", val),
        None => eprintln!("There is no sum"),
    }
    //ANCHOR_END: option_map
}
