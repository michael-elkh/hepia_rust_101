//! Contains the core logic of the library, allowing to tore generic values
//! (or their absence) and manipulate them.
//! We demonstrates three kind of way to deal with errors

use crate::binary_operator::BinaryOperator;

/// Computes the result of a binary reduction of an Array of a type T.
/// Take the binary operation as a function.
/// Returns a [Option::Some] containing the result value
/// or [Option::None] if the array was empty.
///
/// # Example
///
/// ```
/// # use closures::find::{find_with_hof};
/// # fn main() {
/// let tab = [10, 32, 12, 43, 52, 53, 83, 2, 9];
/// let min = find_with_hof(&tab,|x, y| if x <= y { x } else { y });
/// assert!(min == Some(2));
/// # }
/// ```
// ANCHOR: find_with_hof
pub fn find_with_hof<T: Copy>(tab: &[T], op: BinaryOperator<T>) -> Option<T> {
    let mut res = None;
    // Here is T is Copyable. Which means that t is not moved in the loop
    for t in tab {
        if let Some(val) = res {
            res = Some(op(val, *t))
        } else {
            res = Some(*t)
        }
    }
    res
}
// ANCHOR_END: find_with_hof
