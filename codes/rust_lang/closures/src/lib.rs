//! This crate shows us different ways of dealing with errors in a Rust program.
//! You will find examples of [Option], [Result] and [panic!].

pub mod binary_operator;
pub mod find;
pub mod io;

#[cfg(test)]
mod tests {
    use crate::binary_operator::*;
    use crate::find::find_with_hof;
    const TAB: [i32; 9] = [10, 32, 12, 43, 52, 53, 83, 2, 9];
    const TAB_EMPTY: [i32; 0] = [];
    const MIN_TAB: i32 = 2;
    const MAX_TAB: i32 = 83;

    #[test]
    fn test_find_with_option_min() {
        let min: Option<i32> = find_with_hof(&TAB, |x: i32, y: i32| if x <= y { x } else { y });

        assert!(min == Some(MIN_TAB));
    }

    #[test]
    fn test_find_with_option_max() {
        let max: Option<i32> = find_with_hof(&TAB, |x: i32, y: i32| if x >= y { x } else { y });

        assert!(max == Some(MAX_TAB));
    }

    #[test]
    fn test_find_with_option_empty() {
        let min: Option<i32> =
            find_with_hof(&TAB_EMPTY, |x: i32, y: i32| if x <= y { x } else { y });

        assert!(min.is_none());
    }

    #[test]
    fn test_minimum_operator() {
        let f = minimum_operator::<i32>();

        assert!(f(5, 10) == 5);
    }

    #[test]
    fn test_maximum_operator() {
        let f = maximum_operator::<i32>();

        assert!(f(5, 10) == 10);
    }

    #[test]
    fn test_sum_operator() {
        let f = sum_operator::<i32>();

        assert!(f(5, 10) == 15);
    }

    #[test]
    fn test_mul_operator() {
        let f = mul_operator::<i32>();

        assert!(f(5, 10) == 50);
    }
}
