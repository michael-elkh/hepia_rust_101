//! This crate shows us different ways of dealing with errors in a Rust program.
//! You will find examples of [Option], [Result] and [panic!].

pub mod find;
pub mod io;

#[cfg(test)]
mod tests {
    use crate::find::{find_absolute_minimum, find_minimum};
    const VEC: [i32; 9] = [10, 32, 12, 43, 52, 53, 83, 2, 9];
    const VEC_2: [i32; 9] = [-10, 32, 12, -43, 52, -53, 83, -2, 9];
    const MIN_VEC: i32 = 2;
    const ABS_MIN_VEC_2: i32 = -2;

    #[test]
    fn test_find_minimum() {
        let min: Option<i32> = find_minimum(&(VEC.to_vec()));

        assert!(min == Some(MIN_VEC));
    }

    #[test]
    fn test_find_absolute_minimum() {
        let min: Option<i32> = find_absolute_minimum(&(VEC_2.to_vec()));

        assert!(min == Some(ABS_MIN_VEC_2));
    }
}
