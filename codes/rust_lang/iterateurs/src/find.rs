//! Contains the core logic of the library, allowing to tore generic values
//! (or their absence) and manipulate them.
//! We demonstrates three kind of way to deal with errors

/// Computes the minimum of a vector of i32.
/// Returns a [Option::Some] containing the minimum value
/// or [Option::None] if the vec was empty.
///
/// # Example
///
/// ```
/// # use iterateurs::find::{find_minimum};
/// # fn main() {
/// let v = vec![-2, 5, 18, 65, 22, 56, -30];
/// let min = find_minimum(&v);
/// assert!(min == Some(-30));
/// # }
/// ```
// ANCHOR: find_minimum
pub fn find_minimum(v: &Vec<i32>) -> Option<i32> {
    v.iter().fold(None, |acc, current| {
        let next_acc = if let Some(val) = acc {
            if val <= *current {
                val
            } else {
                *current
            }
        } else {
            *current
        };
        Some(next_acc)
    })
}
// ANCHOR_END: find_minimum

/// Computes the smallest even number in a vector of i32.
/// Returns a [Option::Some] containing the smallest even number
/// or [Option::None] if the vec was empty.
///
/// # Example
///
/// ```
/// # use iterateurs::find::{find_even_minimum};
/// # fn main() {
/// let v = vec![15, 64, 47, 2, 1, 53, 22];
/// let min = find_even_minimum(&v);
/// assert!(min == Some(2));
/// # }
/// ```
// ANCHOR: find_even_minimum
pub fn find_even_minimum(v: &Vec<i32>) -> Option<i32> {
    v.iter().filter(|i| *i % 2 == 0).fold(None, |acc, current| {
        let next_acc = if let Some(val) = acc {
            if val <= *current {
                val
            } else {
                *current
            }
        } else {
            *current
        };
        Some(next_acc)
    })
}
// ANCHOR_END: find_even_minimum

/// Computes the minimum absolute value of a vector of i32.
/// Returns a [Option::Some] containing the minimum abs value
/// or [Option::None] if the vec was empty.
///
/// # Example
///
/// ```
/// # use iterateurs::find::{find_absolute_minimum};
/// # fn main() {
/// let v = vec![-2, 5, 18, 65, 22, 56, -30];
/// let min = find_absolute_minimum(&v);
/// assert!(min == Some(-2));
/// # }
/// ```
// ANCHOR: find_absolute_minimum
pub fn find_absolute_minimum(v: &Vec<i32>) -> Option<i32> {
    // ANCHOR: find_absolute_minimum_1
    let signs = v.iter().map(|i| i.signum());
    let abs_values = v.iter().map(|i| i.abs());
    // ANCHOR_END: find_absolute_minimum_1
    // ANCHOR: find_absolute_minimum_2
    signs
        .zip(abs_values)
        // ANCHOR_END: find_absolute_minimum_2
        // ANCHOR: find_absolute_minimum_3
        .fold(None, |acc, (c_sign, c_abs_v)| {
            let next_acc = if let Some((sign, abs_v)) = acc {
                if abs_v <= c_abs_v {
                    (sign, abs_v)
                } else {
                    (c_sign, c_abs_v)
                }
            } else {
                (c_sign, c_abs_v)
            };
            Some(next_acc)
        })
        // ANCHOR_END: find_absolute_minimum_3
        // ANCHOR: find_absolute_minimum_4
        .map(|(sign, abs_v)| sign * abs_v)
    // ANCHOR_END: find_absolute_minimum_4
}
// ANCHOR_END: find_absolute_minimum
