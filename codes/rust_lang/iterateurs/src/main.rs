use iterateurs::find::{find_absolute_minimum, find_even_minimum, find_minimum};
use iterateurs::io;

fn main() {
    let v = io::read_command_line_correct();
    println!("Among the elements in the list:");
    io::print_vec(&v);

    let min = find_minimum(&v);
    match min {
        Some(val) => println!("The minimum value is {}", val),
        None => eprintln!("There is no minimum"),
    }

    let min = find_absolute_minimum(&v);
    match min {
        Some(val) => println!("The minimum by absolue value is {}", val),
        None => eprintln!("There is no minimum"),
    }

    let min = find_even_minimum(&v);
    match min {
        Some(val) => println!("The smallest even value is {}", val),
        None => eprintln!("There is no minimum"),
    }
}
