//! Contains functions to interact with the user, either
//! by reading inputs from the terminal, either by writing values
//! in it.

/// Poorly emulates the parsing of a command line.
pub fn read_command_line_correct() -> Vec<i32> {
    vec![-10, 32, 12, -43, 52, -53, 83, -2, 9]
}

/// Poorly emulates the parsing of a command line.
pub fn read_empty_command_line() -> Vec<i32> {
    vec![]
}

/// Prints all the elements of the vector.
/// vector is borrowed here
pub fn print_vec(v: &Vec<i32>) {
    print!("[ ");
    for t in v {
        print!("{} ", t);
    }
    println!("]");
}
