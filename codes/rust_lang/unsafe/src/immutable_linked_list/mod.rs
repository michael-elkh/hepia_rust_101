// ANCHOR: element
struct Element {
    data: i32,
    next: Option<Box<Element>>,
}
// ANCHOR_END: element

impl Element {
    fn new(data: i32, next: Option<Box<Element>>) -> Self {
        Element { data, next }
    }
}

// ANCHOR: linked_list
pub struct LinkedList {
    head: Option<Box<Element>>,
}
// ANCHOR_END: linked_list

impl LinkedList {
    // ANCHOR: new
    pub fn new() -> Self {
        Self { head: None }
    }
    // ANCHOR_END: new

    // ANCHOR: is_empty
    pub fn is_empty(self) -> (bool, Self) {
        match self.head {
            None => (true, self),
            _ => (false, self),
        }
    }
    // ANCHOR_END: is_empty

    // ANCHOR: push
    pub fn push(self, data: i32) -> Self {
        let elem = Box::new(Element::new(data, self.head));
        Self { head: Some(elem) }
    }
    // ANCHOR_END: push

    // ANCHOR: pop
    pub fn pop(self) -> (Option<i32>, Self) {
        if let Some(elem) = self.head {
            (Some(elem.data), Self { head: elem.next })
        } else {
            (None, Self { head: None })
        }
    }
    // ANCHOR_END: pop

    // ANCHOR: print
    pub fn print(self) -> Self {
        let mut new_list = Self::new();
        // ANCHOR: while
        let mut current = self.head;
        while let Some(tmp) = current {
            print!("{} --> ", tmp.data);
            new_list = new_list.push(tmp.data);
            current = tmp.next;
        }
        println!("∅");
        // ANCHOR_END: while
        new_list
    }
    // ANCHOR_END: print

    #[allow(dead_code)]
    // ANCHOR: clear
    pub fn clear(self) {
        let mut current = self.head;
        while let Some(tmp) = current {
            current = tmp.next;
        }
    }
    // ANCHOR_END: clear
}

#[cfg(test)]
mod tests {
    use super::LinkedList;

    #[test]
    fn new() {
        let (is_empty, _) = LinkedList::new().is_empty();
        assert!(is_empty);
    }

    #[test]
    fn push() {
        let list = LinkedList::new();
        let list = list.push(1);
        let (is_empty, list) = list.is_empty();
        assert!(!is_empty);
        assert_eq!(list.head.as_ref().unwrap().data, 1);

        let list = list.push(2);
        assert_eq!(list.head.unwrap().data, 2);
    }

    #[test]
    fn pop() {
        let list = LinkedList::new();
        let (e, list) = list.pop();
        assert_eq!(e, None);

        let list = list.push(1);
        let (e, list) = list.pop();
        assert_eq!(e, Some(1));

        let (e, list) = list.pop();
        assert_eq!(e, None);

        let list = list.push(2);
        let list = list.push(3);
        let list = list.push(4);

        assert_eq!(list.head.as_ref().unwrap().data, 4);

        let (e, list) = list.pop();
        assert_eq!(list.head.as_ref().unwrap().data, 3);
        assert_eq!(e, Some(4));
        let (_, list) = list.pop();
        let (_, list) = list.pop();
        let (is_empty, _) = list.is_empty();
        assert!(is_empty);
    }
}
