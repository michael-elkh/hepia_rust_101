use linked_list::immutable_linked_list::LinkedList as ImmutableList;
use linked_list::safe_linked_list::LinkedList as SafeList;
use linked_list::unsafe_linked_list::LinkedList as UnsafeList;

fn create_lists() -> (ImmutableList, SafeList, UnsafeList) {
    (ImmutableList::new(), SafeList::new(), UnsafeList::new())
}

fn main() {
    let (immutable_list, mut safe_list, mut unsafe_list) = create_lists();

    // Populate lists
    let immutable_list = immutable_list.push(1);
    let immutable_list = immutable_list.push(2);
    let immutable_list = immutable_list.push(3);

    safe_list.push(1);
    safe_list.push(2);
    safe_list.push(3);

    unsafe_list.push(1);
    unsafe_list.push(2);
    unsafe_list.push(3);

    let (i_val, immutable_list) = immutable_list.pop();
    let s_val = safe_list.pop();
    let u_val = unsafe_list.pop();

    assert_eq!(i_val, s_val);
    assert_eq!(i_val, u_val);
    assert_eq!(s_val, u_val);

    let immutable_list = immutable_list.push(4);
    safe_list.push(4);
    unsafe_list.push(4);

    immutable_list.print();
    safe_list.print();
    unsafe_list.print();

    for _j in 1..1000 {
        let mut ul = UnsafeList::new();
        for i in 1..1_000_000 {
            ul.push(i);
        }
    }

    unsafe_list.print();
}
