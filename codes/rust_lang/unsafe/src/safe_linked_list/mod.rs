// ANCHOR: element
struct Element {
    data: i32,
    next: Option<Box<Element>>,
}
// ANCHOR_END: element

impl Element {
    fn new(data: i32, next: Option<Box<Element>>) -> Self {
        Element { data, next }
    }
}

// ANCHOR: linked_list
pub struct LinkedList {
    head: Option<Box<Element>>,
}
// ANCHOR_END: linked_list

impl LinkedList {
    // ANCHOR: new
    pub fn new() -> Self {
        Self { head: None }
    }
    // ANCHOR_END: new

    // ANCHOR: is_empty
    pub fn is_empty(&self) -> bool {
        self.head.is_none()
    }
    // ANCHOR_END: is_empty

    // ANCHOR: push
    pub fn push(&mut self, data: i32) {
        // let new_element = Box::new(Element::new(data, self.head));
        // Cela ne peut pas fonctionner, pace qu'on est derrière une référence partagée
        // et donc on peut pas "move" self.head

        // ANCHOR: take
        let new_head = Box::new(Element::new(data, self.head.take()));
        // ANCHOR_END: take
        // take retourne la valeur qui se trouve dans Some et laisse un None
        // à la place de l'option.
        // C'est strictement équivalent au replace (ci-dessous)
        self.head = Some(new_head);
    }
    // ANCHOR_END: push

    // ANCHOR: push_replace
    pub fn push_replace(&mut self, data: i32) {
        // ANCHOR: replace
        let old_head = std::mem::replace(&mut self.head, None);
        let new_head = Box::new(Element::new(data, old_head));
        // ANCHOR: replace
        // replace retourne self.head et remplace l'ancienne valeur par None (comme ça le compilateur est content)
        self.head = Some(new_head);
    }
    // ANCHOR_END: push_replace

    // ANCHOR: push_unsafe
    pub fn push_unsafe(&mut self, data: i32) {
        let old_head = unsafe {
            // De la documentation:
            // `read` crée une copie bit à bit de `T`, que `T` soit [`Copy`] ou non.
            // Si `T` n'est pas [`Copy`], utiliser à la fois la valeur renvoyée et la valeur de
            // `*src` peut violer la sécurité de la mémoire. Notez que l'assignation à `*src` compte comme une
            // utilisation parce qu'elle tentera de `drop` la valeur à `*src`.
            let result = std::ptr::read(&self.head);
            std::ptr::write(&mut self.head, None);
            // Ce `write` est en fait un "truc" pour enlever l'aliasing entre
            // self.head et result. Il écrase la valeur à self.head avec None
            // sans `drop` self.head et donc result.
            result
        };
        let new_head = Box::new(Element::new(data, old_head));
        self.head = Some(new_head);
    }
    // ANCHOR_END: push_unsafe

    // ANCHOR: pop
    pub fn pop(&mut self) -> Option<i32> {
        // map prend la valeur dans Some, lui applique la fonction anonyme
        // et remballe la valeur obtenue dans un Some. Si l'Option
        // originale est None, il se passe rien.
        self.head.take().map(|element| {
            self.head = element.next;
            element.data
        })
    }
    // ANCHOR_END: pop

    // ANCHOR: print
    pub fn print(&self) {
        let mut current = &self.head;
        while let Some(tmp) = &current {
            print!("{} --> ", tmp.data);
            current = &tmp.next;
        }
        println!("∅");
    }
    // ANCHOR_END: print
}

impl Drop for LinkedList {
    fn drop(&mut self) {
        let mut current = self.head.take();
        while let Some(mut tmp) = current {
            current = tmp.next.take();
        }
    }
}

#[cfg(test)]
mod test {
    use super::LinkedList;

    #[test]
    fn new() {
        assert!(LinkedList::new().is_empty());
    }

    #[test]
    fn push() {
        let mut list = LinkedList::new();
        list.push(1);
        assert_eq!(list.head.as_ref().unwrap().data, 1);
        list.push(2);
        assert_eq!(list.head.as_ref().unwrap().data, 2);
    }

    #[test]
    fn pop() {
        let mut list = LinkedList::new();
        let e = list.pop();
        assert_eq!(e, None);

        list.push(1);
        let e = list.pop();
        assert_eq!(e, Some(1));

        let e = list.pop();
        assert_eq!(e, None);

        list.push(2);
        list.push(3);
        list.push(4);

        assert_eq!(list.head.as_ref().unwrap().data, 4);

        let e = list.pop();
        assert_eq!(list.head.as_ref().unwrap().data, 3);
        assert_eq!(e, Some(4));
        list.push(5);
        list.push(6);
        let e = list.pop();
        assert_eq!(list.head.as_ref().unwrap().data, 5);
        assert_eq!(e, Some(6));
    }
}
