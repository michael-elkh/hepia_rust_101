use std::alloc::{alloc, dealloc, handle_alloc_error, Layout};
use std::ptr;

// ANCHOR: element
struct Element {
    data: i32,
    next: *mut Element,
}
// ANCHOR_END: element

impl Element {
    // ANCHOR: new
    fn new(data: i32, next: *mut Element) -> *mut Element {
        let layout = Layout::new::<Element>();
        let e = unsafe { alloc(layout) as *mut Element };
        if e.is_null() {
            handle_alloc_error(layout);
        }
        unsafe {
            (*e).data = data;
            (*e).next = next;
        }
        e
    }
    // ANCHOR_END: new
}

//ANCHOR: drop
impl Drop for Element {
    fn drop(&mut self) {
        let elem = self as *mut Element;
        if !elem.is_null() {
            let layout = Layout::new::<Element>();
            unsafe {
                dealloc(elem as *mut u8, layout);
            }
        }
    }
}
//ANCHOR_END: drop

// ANCHOR: linked_list
pub struct LinkedList {
    head: *mut Element,
}
// ANCHOR_END: linked_list

impl LinkedList {
    // ANCHOR: ll_new
    pub fn new() -> LinkedList {
        LinkedList {
            head: ptr::null_mut(),
        }
    }
    // ANCHOR_END: ll_new

    // ANCHOR: is_empty
    fn is_empty(&self) -> bool {
        self.head.is_null()
    }
    // ANCHOR_END: is_empty

    // ANCHOR: push
    pub fn push(&mut self, data: i32) {
        let new_head = Element::new(data, self.head);
        self.head = new_head;
    }
    // ANCHOR_END: push

    // ANCHOR: pop
    pub fn pop(&mut self) -> Option<i32> {
        if self.is_empty() {
            None
        } else {
            let old_head = self.head;
            unsafe {
                self.head = (*self.head).next;
            }
            let val = unsafe { (*old_head).data };
            unsafe {
                old_head.drop_in_place();
            }
            Some(val)
        }
    }
    // ANCHOR_END: pop

    // ANCHOR: print
    pub fn print(&self) {
        let mut current_head = self.head;
        while !current_head.is_null() {
            unsafe {
                print!("{} --> ", (*current_head).data);
                current_head = (*current_head).next;
            }
        }
        println!("∅");
    }
    // ANCHOR_END: print
}

// ANCHOR: ll_drop
impl Drop for LinkedList {
    fn drop(&mut self) {
        while !self.is_empty() {
            let _ = self.pop();
        }
    }
}
// ANCHOR_END: ll_drop

#[cfg(test)]
mod tests {
    use super::LinkedList;

    #[test]
    fn new() {
        assert!(LinkedList::new().is_empty());
    }

    #[test]
    fn push() {
        let mut list = LinkedList::new();
        list.push(1);
        assert_eq!(unsafe { (*list.head).data }, 1);
        list.push(2);
        assert_eq!(unsafe { (*list.head).data }, 2);
    }

    #[test]
    fn pop() {
        let mut list = LinkedList::new();
        let e = list.pop();
        assert_eq!(e, None);

        list.push(1);
        let e = list.pop();
        assert_eq!(e, Some(1));

        let e = list.pop();
        assert_eq!(e, None);

        list.push(2);
        list.push(3);
        list.push(4);

        assert_eq!(unsafe { (*list.head).data }, 4);

        let e = list.pop();
        assert_eq!(unsafe { (*list.head).data }, 3);
        assert_eq!(e, Some(4));
        list.push(5);
        list.push(6);
        let e = list.pop();
        assert_eq!(unsafe { (*list.head).data }, 5);
        assert_eq!(e, Some(6));
    }
}
