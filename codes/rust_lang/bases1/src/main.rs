/// Rust basics:
///     - basic types (usize, i32, str)
///     - const, let, let mut
///     - control structures (for, if)
///     - collection: arrays, indexing
///     - macros for IO and errors

// No functions, only types used are i32, usize and [i32; SIZE].
// Indexing of array, for and if else control structures.
// Macros: panic! for error handling, print! and println! for I/O.
// Clippy does not like the code at all.

fn main() {
    const SIZE: usize = 9;
    let tab: [i32; SIZE] = [10, 32, 12, 43, 52, 53, 83, 2, 9];

    if SIZE == 0 {
        panic!("Size is of tab = 0.");
    }

    println!("Among the numbers in the list:");
    for i in 0..SIZE {
        print!("{} ", tab[i]);
    }
    println!();

    let mut min = tab[0];
    for i in 1..SIZE {
        if min > tab[i] {
            min = tab[i];
        }
    }

    println!("The minimal value is: {}", min);
}
