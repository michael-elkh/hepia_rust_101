use cli::io;

fn main() -> Result<(), String> {
    io::read_command_line_builder()?;
    Ok(())
}
