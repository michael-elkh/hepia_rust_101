use std::fmt::Display;

use crate::minimum::Minimum;

/// An generic enumerated type that encapsulates and Option<T>.
#[derive(Clone, Copy)]
pub struct SomethingOrNothing<T>(Option<T>);

impl<T: Minimum + Display> SomethingOrNothing<T> {
    pub fn new(val: T) -> Self {
        SomethingOrNothing(Some(val))
    }
    /// A static function that prints the content of a SomethingOrNothing.
    pub fn to_string(&self) -> String {
        match self.0 {
            None => String::from("Nothing."),
            Some(val) => format!("Something is: {}", val),
        }
    }
}

impl<T> Default for SomethingOrNothing<T> {
    /// By Default a [SomethingOrNothing] is a nothing.
    fn default() -> Self {
        SomethingOrNothing(None)
    }
}

impl<T: PartialEq + Minimum> PartialEq for SomethingOrNothing<T> {
    fn eq(&self, other: &Self) -> bool {
        match (self.0, other.0) {
            (None, None) => true,
            (Some(lhs), Some(rhs)) => lhs == rhs,
            _ => false,
        }
    }
}

impl<T: Minimum + Display> Minimum for SomethingOrNothing<T> {
    fn min(self, rhs: Self) -> Self {
        match (self.0, rhs.0) {
            (None, None) => SomethingOrNothing(None),
            (Some(lhs), Some(rhs)) => SomethingOrNothing::new(lhs.min(rhs)),
            (None, Some(rhs)) => SomethingOrNothing::new(rhs),
            (Some(lhs), None) => SomethingOrNothing::new(lhs),
        }
    }
}

/// Computes the minimum of an Array of a type T which implements the [Minimum] trait.
/// Returns a [Some] containing the the minimum value
/// or [None] if no minimum value was found.
///
/// # Examples
///
/// ```
/// # use cli::something_or_nothing::{SomethingOrNothing, find_min};
/// # fn main() {
/// let tab = vec![10, 32, 12, 43, 52, 53, 83, 2, 9];
/// let min = find_min(&tab);
/// assert!(min == SomethingOrNothing::new(2));
/// # }
/// ```
///
/// ```
/// # use cli::something_or_nothing::{SomethingOrNothing, find_min};
/// # fn main() {
/// let tab: Vec<i32> = vec![];
/// let min = find_min(&tab);
/// assert!(min == SomethingOrNothing::default());
/// # }
/// ```
pub fn find_min<T: Minimum + Display>(tab: &[T]) -> SomethingOrNothing<T> {
    let mut minimum: SomethingOrNothing<T> = SomethingOrNothing(None);
    // Here is T is Copyable. Which means that t is not moved in the loop
    for t in tab {
        minimum = minimum.min(SomethingOrNothing::new(*t));
    }
    minimum
}
