use std::{
    fs::File,
    io::{BufReader, Read, Write},
};

// ANCHOR: use_clap
use clap::{value_parser, Arg, Command, Parser};
// ANCHOR_END: use_clap

// ANCHOR: consts
const COMMAND: &str = "cli";
const AUTHOR: &str = "Orestis Malaspinas";
const VERSION: &str = "0.1.0";
// ANCHOR_END: consts

use crate::something_or_nothing::find_min;

// ANCHOR: read_from_urandom
fn read_from_urandom(count: usize) -> Result<Vec<i32>, String> {
    // ANCHOR: open
    let file = File::open("/dev/urandom").map_err(|_| "Could not open /dev/urandom")?;
    // ANCHOR_END: open
    // ANCHOR: read
    let mut buf_reader = BufReader::new(file);
    let mut numbers = vec![0; count * 4];
    buf_reader
        .read_exact(&mut numbers)
        .map_err(|_| "Could not read numbers")?;
    // ANCHOR_END: read
    // ANCHOR: convert_to_i32
    Ok(numbers
        .chunks(4)
        .map(|i| i32::from_be_bytes(i.try_into().unwrap()))
        .collect::<Vec<_>>())
    // ANCHOR_END: convert_to_i32
}
// ANCHOR_END: read_from_urandom

// ANCHOR: write_to_file
fn write_to_file(output: &str, numbers: &[i32]) -> Result<(), String> {
    // ANCHOR: create
    let mut file = File::create(output).map_err(|_| format!("Failed to create {output}"))?;
    // ANCHOR_END: create
    // ANCHOR: write
    writeln!(file, "Among the Somethings in the list:")
        .map_err(|_| "Failed to write header into file.")?;
    for n in numbers {
        write!(file, "{n} ").map_err(|_| format!("Failed to write {n} into file."))?;
    }
    writeln!(file,).map_err(|_| "Failed to write carriage return into file.")?;
    writeln!(file, "{}", find_min(numbers).to_string())
        .map_err(|_| "Failed to write minimum value into file.")?;
    // ANCHOR_END: write
    Ok(())
}
// ANCHOR_END: write_to_file

/// Reads i32 from the command line and returns a [Vec] containing
/// these numbers. Returns errors when the parsing fails.
pub fn read_command_line_builder() -> Result<(), String> {
    // ANCHOR: matches
    let matches =
    // ANCHOR: new_command
        Command::new(COMMAND)
            .author(AUTHOR)
            .version(VERSION)
    // ANCHOR_END: new_command
    // ANCHOR: new_args
            .arg(
                Arg::new("numbers") // id
                    .short('n')         // version courte -n
                    .long("numbers")    // ou longue --numbers
                    .help("A list of i32 numbers") // l'aide
                    .num_args(1..) // combien il y a d'entrées
                    .allow_negative_numbers(true) // on peut avoir des négatifs
                    .value_parser(value_parser!(i32)) // on veut s'assurer que ça soit des nombres
                    .required(false), // optionnel
            )
            .arg(
                Arg::new("count")
                    .short('c')
                    .long("count")
                    .help("How many random numbers we want?")
                    .value_parser(value_parser!(usize))
                    .conflicts_with("numbers") // impossible d'avoir -c et -n
                    .required(false),
            )
            .arg(
                Arg::new("output")
                    .short('o')
                    .long("output")
                    .help("Should we write output in a file?")
                    .required(false),
            )
    // ANCHOR: new_args
            .get_matches();
    // ANCHOR_END: matches

    // ANCHOR: numbers_matches
    let numbers = if let Some(count) =
        // ANCHOR: get_one_matches
        matches.get_one::<usize>("count")
    // ANCHOR_END: get_one_matches
    {
        read_from_urandom(*count)?
    } else if let Some(numbers) =
        // ANCHOR: get_many_matches
        matches.get_many::<i32>("numbers")
    // ANCHOR_END: get_many_matches
    {
        numbers.copied().collect()
    } else {
        Vec::new()
    };
    // ANCHOR_END: numbers_matches

    // ANCHOR: output_matches
    if let Some(output) =
        // ANCHOR: get_one_string_matches
        matches.get_one::<String>("output")
    // ANCHOR_END: get_one_string_matches
    {
        write_to_file(output, &numbers)?;
    } else {
        println!("Among the Somethings in the list:");
        print_tab(&numbers);
        println!("{}", find_min(&numbers).to_string());
    }
    // ANCHOR_END: output_matches

    Ok(())
}

/// Does not compile without the feature derive
// ANCHOR: derive
#[derive(Parser)]
// ANCHOR: command
#[command(author, version, about, long_about = None)]
// ANCHOR_END: command
struct CliMin {
    // ANCHOR: arg
    #[arg(short, long, help = "A list of i32 numbers", num_args=1.., allow_negative_numbers=true, value_parser = clap::value_parser!(i32))]
    numbers: Option<Vec<i32>>,
    // ANCHOR_END: arg
    #[arg(short, long, help = "How many random numbers we want?", value_parser = clap::value_parser!(usize), conflicts_with = "numbers")]
    count: Option<usize>,
    #[arg(short, long, help = "Filename for writing the numbers.")]
    output: Option<String>,
}
// ANCHOR_END: derive

/// Reads i32 from the command line and returns a [Vec] containing
/// these numbers. Returns errors when the parsing fails.
// ANCHOR: read_command_line_derive
pub fn read_command_line_derive() -> Result<(), String> {
    // ANCHOR: parse
    let cli = CliMin::parse();
    // ANCHOR_END: parse
    let numbers = if let Some(count) = cli.count {
        read_from_urandom(count)?
    } else if let Some(numbers) = cli.numbers {
        numbers
    } else {
        Vec::new()
    };
    if let Some(output) = cli.output {
        write_to_file(&output, &numbers)?;
    } else {
        println!("Among the Somethings in the list:");
        print_tab(&numbers);
        println!("{}", find_min(&numbers).to_string());
    }
    Ok(())
}
// ANCHOR_END: read_command_line_derive

/// Prints all the elements of the `tab`.
/// Tab is borrowed here
pub fn print_tab(tab: &Vec<i32>) {
    for t in tab {
        print!("{} ", t);
    }
    println!();
}
