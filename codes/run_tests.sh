#!/bin/sh

make -C c_lang/min_list
cd rust_lang
for d in *; do
    echo ==================== Running cargo run for $d  ====================
    cargo run --manifest-path $d/Cargo.toml
    echo ==================== Running cargo test for $d ====================
    cargo test --manifest-path $d/Cargo.toml --workspace --verbose
    echo ==================== Running cargo doc for $d  ====================
    cargo doc --manifest-path $d/Cargo.toml
    echo ==================== Running cargo fmt for $d  ====================
    cargo fmt --all --manifest-path $d/Cargo.toml -- --check
done
