# Exercices avancés

Les exercices que nous proposons ici sont très vagues. L'objectif est de vous donner des thèmes,
de choses que vous connaissez du cours de programmation et d'algorithmique de l'année passée
et de les implémenter un peu comme vous voulez sans contraintes particulières pour
que vous expérimentiez avec le langage (oui vous êtes des grand·e·s maintenant).

Essayez au maximum d'utiliser les concepts de Rust vus en classe.

Vous trouverez une liste ci-dessous, avec un degré de difficulté croissant (plus ou moins).
L'idée est d'avancer à votre rythme (ce qui n'est pas équivalent à regarder le plafond
en attendant que ça passe) et vous donner des idées d'exercices devenant plus complexes
au fur et à mesure de votre périple.

- Le nombre secret: implémenter le jeu du nombre secret qui serait joué par des IAs utilisant des stratégies différentes.
- Calculer pi: avec la méthode des fléchettes.
- Une pile simple: implémenter une pile utilisant un tableau statique comme structure de données.
- Un tri: un tri à bulles, tri à deux piles et/ou un quicksort ça fait l'affaire.
- Couverture de la reine: écrire un code qui permet de trouver pour une position de départ aléatoire toutes les solutions de couverture de la reine pour un tableau 8x8.
- Les fractions: écrire une librairie de fractions en utilisant les trait `Add`, `Sub`, etc.
- Les vecteurs: écrire une librairie de vecteurs en utilisant les trait `Add`, `Sub`, etc.
- Le jeu du pendu: implémenter le jeu du pendu où les mots sont des chaînes de caractères aléatoires et les lettres tirées sont également tirées au hasard.
- Compter différemment: écrire un programme qui permet de compter comme au tennis où chaque joueur gagne un point avec une probabilité donnée.
- Le puissance 4: simulez le jeu du puissance 4.
- Le jeu de la bataille: simulez le jeu de carte de la bataille entre deux machines.

## Aller plus haut

- Ascii art: écrire un convertisseur d'image en ASCII art.
- Simulation du système solaire: écrire un simulateur de système solaire.
