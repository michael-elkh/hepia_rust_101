# Préquel

- [] Installation du cours.

# Jour

- [] Lundi matin (9h-10h15, 10h45-12h): 
    - Vérification install, et Hello world.
    - bases1 à bases2
- [] Lundi après-midi (13h-14h15, 14h45-16h): 
    - bases2
- [] Mardi matin (9h-10h15, 10h45-12h):
    - gen_types_composes - propriete
- [] Mardi après-midi (13h-14h15, 14h45-16h): 
    - modules_visibilite - tooling
- [] Mercredi matin (9h-10h15, 10h45-12h): 
    - Gestion d'erreurs
    - Closures
- [] Mercredi après-midi (13h-14h15, 14h45-16h): 
    - Collections (Vec, String), Exos
- [] Jeudi matin (9h-10h15, 10h45-12h): 
    - Itérateurs
    - Lifetimes
- [] Jeudi après-midi (13h-14h15, 14h45-16h): 
    - CLI
- [] Vendredi matin (9h-10h15, 10h45-12h): 
    - Unsafe Rust
- [] Vendredi après-midi (13h-14h15, 14h45-16h): 
    - Choix du projet et groupes

# Soir

Lundi (17h-21h): Vérification install, intro, bases1, bases2
Mardi (17h-21h): gen_types_composes & propriete
Mercredi (17h-21h): part 05 & part 06
Jeudi (17h-21h): Gestion d'erreurs, Lambda, Collections & Itérateurs
