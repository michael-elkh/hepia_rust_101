# Installation

## La chaîne de compilation

<https://www.rust-lang.org/tools/install>

## Rustup

- Outil pour configurer et manager la chaîne de compilation de Rust.

	<https://www.rustup.rs>

- Installation de la chaîne de compilation (toolchain):

```sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

- Quelques commandes importantes

```sh
rustup install stable # installation de la chaîne stable
rustup self uninstall # désinstallation
rustup default stable # chaîne par défaut
rustup update # mise à jour de la chaîne de compilation
rustup doc # documentation dans le navigateur
```

## Contenu de la chaîne de compilation

- `rustc`: compilateur et éditeur de lien
- `cargo`: outil pour la compilation et gestion de dépendances
- `rustdoc`: documentation
- `rust-(lldb|gdb)`: débugger

## `rustc`

- `rustc` est écrit en Rust (en grande partie).
- Pour terster l'installation 
```sh
rustc --help
```

## Mon premier `Hello World!`

- Dans un fichier `hello_world.rs`.

```rust
fn main() { // présent dans tout programme Rust
    println!("Hello, World!"); // macro d'affichage
}
```

- Pour compiler, éditer les liens et exécuter

```console
$ rustc hello_world.rs
$ ./hello_world
Hello, World!
```

## Cargo

- La documentation [Cargo Manifest docs](http://doc.crates.io/manifest.html).
- Cargo est l'outil de compilation et gestion de dépendances de Rust. 
- Indépendant de la version de `rustc`.
- Permet l'installation d'outils (peut se faire avec rustup également)

```console
$ cargo install clippy # conseil de "beauté du code"
$ cargo install rustfmt # formattage automatique du code
```


## Exemple d'utilisation

```console
$ cargo new hello_world --bin # création projet
$ cd hello_world
$ cargo build # cargo check
   Compiling hello_world v0.1.0 (file:///home/malaspor/hello_world)
    Finished dev [unoptimized + debuginfo] target(s) in 1.33s
$ cargo run
    Finished dev [unoptimized + debuginfo] target(s) in 0.05s
     Running `target/debug/hello_world`
Hello, world!
$ ./target/debug/hello_world
Hello, world!
$ cargo build --release # compilation avec optimisations
   Compiling hello_world v0.1.0 (file:///home/malaspor/Downloads/hello_world)
    Finished release [optimized] target(s) in 0.35s
```

## Le fichier `Cargo.toml`:

```toml
[package]
name = "hello_world"
version = "0.1.0"
authors = ["Your Name you@example.com"]

[dependencies]
```
## Le fichier `Cargo.lock`:

```toml
[[package]]
name = "hello_world"
version = "0.1.0"
```

## Éditeur et plugins

- Nous vous conseillons ici l'éditeur `codium` (**pas vscode**).
- Et l'installation du plugin `rust-analyzer`.

