# Les chaînes de caractères

## Généralités

- Deux types de chaînes de caractères: `str` et `String`:
    - `str`: *string literal* (stocké explicitement dans l'exécutable).
    - `String`: liste dynamique de caractères UTF-8.
- `String` est propriétaire de ses données.
- `str` ne peut être "manipulé" que via `&str`

```rust
let literal = "Le type de literal est &str.";
```

## La structure `String`

```console
ptr       # pointeur sur le tas de char
len       # nombre d'éléments utilisés
capacity  # mémoire allouée
```

## Créer des `String`s

```rust [1|2|3|]
let mut empty = String::new();
let hello = String::from("Hello World!"); // converti depuis un &str
let other_hello = "Hello World!".to_string();
```

## Modifier des `String`s

```rust [1-2|]
let mut s = String::from("Hello");
let s1 = " World!";
s.push_str(&s1); // On peut aussi faire push_str(" World!")
println!("{s}");
```

## Concaténer des `String`s

```rust [1-4|]
let s1 = String::from("Hello");
let s2 = String::from(" World!");
let sum1 = s1 + &s2; // s1 moved
let sum2 = "Hello".to_string() + &s2; // s2 borrowed
println!("{sum1} : {sum2}");
```

## Formatter des `String`s

```rust
let s1 = String::from("Hello");
let s2 = String::from("World");
let formatted = format!("{s1} {s2}!");
println!("{formatted}");
```

## Indexer des `String`

```rust compile_fail
let s = String::from("Hello");
let t = s[0]; // fail
```

```rust
let hello = String::from("Καλημέρα!");
println!("{}", hello.len());
```

## Encodage

* UTF-8 `!=` ASCII: encodage à longueur variable (1 à 4 octets).
* Impossible d'indexer en `O(1)`, car on connaît pas la valeur avant d'avoir lu 1 à 4 octets à chaque fois.

## Mais alors comment faire?

```rust [1-5|6|]
let hello = String::from("Καλημέρα!");
for c in hello.chars() {
    print!("{c}");
}
println!("");
println!("index 3: {}", hello.chars().nth(3).unwrap());
```

## Le slice de `String`

* Une tranche de `String` est une référence vers un bout de chaîne d'UTF-8.
* On peut facilement créer des tranches de `String`... invalides.

```rust should_panic
let hello = String::from("Καλημέρα!");
let h = &hello[0..3];
```

