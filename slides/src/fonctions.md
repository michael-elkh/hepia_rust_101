# Instructions et expressions

## Les instructions (statements)

- **Instruction**: commande effectuant une action mais ne retournant aucune valeur.
    ```rust ignore
    let x = 1; // une instruction
    ```
 <br>

- On ne peut pas assigner une instruction `let`
    ```rust compile_fail
    let y = (let x = 1);
    ```

## Les expressions

- **Expression**: combinaison de variables, opérations, ... retournant une valeur.
    ```c
    3 + 5
    ```
- Presque tout code Rust est une expression.
    ```rust [|2-5|4|]
    fn main() {
        let y = {
            let x = 5 + 3;
            x - 9
        };
        println!("5 + 3 - 9 = {}", y);
    }
    ```

# Fonctions

## Fonction sans argument, sans retour

```rust [1-4|5-7]
fn main() {
    println!("La fonction main est une fonction.");
    fonction_fonction();
}
fn fonction_fonction() {
    println!("La fonction fonction_fonction est une fonction.");
}
```

## Fonction avec argument, sans retour

```rust [1-3|5|]
fn affiche_entier(x: i32) {
    println!("Affiche l'entier {}.", x);
}
fn main() {
    affiche_entier(1024);
}
```

## Valeur de retour de fonctions

```rust [1-3|2|]
fn la_reponse() -> i32 {
    42 // ou return 42;
}
fn main() {
    println!("La réponse est {}.", la_reponse());
}
```

## Une erreur est vite faite

```rust compile_fail
fn la_reponse() -> i32 {
    42;
}
```

## On peut faire plus

```rust [2-6|]
fn la_reponse(univers_et_tout: bool) -> i32 {
    if univers_et_tout {
        42
    } else {
        -1
    }
}
fn main() {
    println!("La réponse_est {}.", la_reponse(true));
}
```

## L'erreur

```console
error[E0308]: mismatched types
 --> src/main.rs:1:20
  |
1 | fn la_reponse() -> i32 {
  |    ----------      ^^^ expected `i32`, found `()`
  |    |
  |    implicitly returns `()` as its body has no tail or `return` expression
2 |     42;
  |       - help: remove this semicolon to return this value

For more information about this error, try `rustc --explain E0308`
```

Une fonction sans retour retourne un `()`.