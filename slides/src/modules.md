# Modules

## Namespaces (*espace de nom*)

- Permet de distinguer deux objets qui ont le même nom.
- Permet de donner une portée aux identificateurs.

## En Rust il y a deux façon de faire

1. Les *crates* qui sont des librairies réutilisables.
2. Les *modules* qui subdivisent les librairies en plus petites parties.

## Les crates

- Une *crate* est une librairie.
- Elle contient en général beaucoup de modules.

## Compilation

- `Cargo`, l'outil de compilation du Rust, va chercher toutes les informations directement dans `main.rs` ou `lib.rs`.
- Les informations sur les modules se trouvent dans ces fichiers.

## Une libraire (`crate`)

- On peut créer une libraire avec `cargo new ma_librairie --lib`.
- Ou alors en créant un fichier `lib.rs` "à côté" du `main.rs`.
- Pour accéder à ce qui se trouve dans la librairie on devra utiliser
	```rust ignore
	use ma_librairie;
	```
- Et appeler les fonctions, enums, ...
	```rust ignore
	ma_librairie::ma_fct();
	ma_librairie::MonEnum;
	```

## Un module:

- est un bloc de code dans une librairie
- qualifie tous les noms qui se trouvent à l'intérieur
- a un module parent (ça peut être la crate elle-même)
- peut avoir des modules enfants

## La définition d'un module en ligne

```rust [1-5|]
mod hepia {
    fn uni_automne() {
        unimplemented!();
    }
}
fn main() {
}
```

## Le contenu d'un module est privé

```rust compile_fail [2-3,6|]
mod hepia {
    fn uni_automne() {
    }
}
fn main() {
    hepia::uni_automne(); // uni_automne() privé
}
```

## Public: `pub`

```rust [2-3,6|]
mod hepia {
    pub fn uni_automne() {
    }
}
fn main() {
    hepia::uni_automne(); // uni_automne() publique
}
```

## Appels

```rust [1,6,7,9|2,8|3,5|4|1-13]
mod hepia { // seul qui a pas besoin de pub
    pub fn uni_automne() { }
    pub mod isc {
        pub struct Prof { }
    }
}
mod heg {
    pub fn uni_automne() { }
}
fn main() {
    let a = hepia::uni_automne();
    let b = heg::uni_automne();
    let c = hepia::isc::Prof {};
}
```

## Structure de fichiers

```console
ecole
├── Cargo.toml
├── src
|   ├── heg.rs
|   ├── hepia
|   |   ├── isc
|   |   |   └── mod.rs
|   │   └── mod.rs
|   ├── lib.rs
|   └── main.rs

```

## Les fichiers et visibilité

```rust ignore [1-3|4-6|7-8|9-10|11-12|13-17|]
// dans lib.rs
pub mod hepia;
pub mod heg;
// dans hepia/mod.rs
pub fn uni_automne() { }
pub mod isc;
// dans hepia/isc/mod.rs
pub struct Prof { }
// dans heg.rs
pub fn ma_fonction() { }
// dans main.rs
use ecole::{hepia, heg}; // imports
fn main() {
    let a = hepia::uni_automne();
    let b = heg::uni_automne();
    let c = hepia::isc::Prof {};
}
```

## Résumé

- Les modules permettent de compartimenter le code.
- Par défaut tout ce qui se trouve dans un module est privé.
- On contrôle la visibilité avec `pub`rust.
- Si un item est public il peut être accédé depuis n'importe quel module parent.
- Si un item est privé il n'est accessible que depuis son parent direct et n'importe lequel de ses modules enfants.
- On peut rendre visible des sous-modules, ou des items avec `use`.

## Plus de finesse

Contrôle beaucoup plus fin possible ([voir ici](https://doc.rust-lang.org/reference/visibility-and-privacy.html)):

```rust,ignore
pub
   | pub ( crate ) // public dans la crate
   | pub ( self )  // public dans le module courant
   | pub ( super ) // public dans le module parent
   | pub ( in path ) // public dans le path explicite
```
