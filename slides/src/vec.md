# `Vec<T>`

## Généralités

- Liste dynamique de valeurs de **même type**: *vecteurs*.
- Garantie de contiguïté en mémoire.
- La taille de la liste peut changer dynamiquement.
- Le type des éléments de `Vec` est générique.
- La documentation du type `Vec` se [trouve ici](https://doc.rust-lang.org/std/vec/struct.Vec.html)

# Fonctionnalités d'un `Vec`

## Création

```rust
fn main() {
    let v: Vec<i32> = Vec::new(); // pas moyen de le connaître le type
    println!("{:?} est de longueur {}", v, v.len()); // v.len() donne la longeur du vecteur
    let v = vec![1, 2, 3, 4]; // type inféré, création à l'aide d'une macro
    println!("{:?} est de longueur {}", v, v.len()); // v.len() donne la longeur du vecteur
}    
```

## Désallocation

```rust
fn main() {
    {
        let v = vec![1, 2, 3, 4];
        // on fait de trucs
    } // v sort de la portée il est détruit
}
```

## Ajouter/ôter des éléments

```rust
fn main() {
    let mut v = Vec::new();
    v.push(1); // type des éléments de v inféré
    v.push(2);
    v.push(3);
    assert_eq!(v, vec![1,2,3]);
    println!("{:?}", v);
    let last = v.pop(); // retourne une Some(i), ou None si vide
    println!("{:?} et le dernier {:?}", v, last);
}
```

# Ownership et `Vec<T>`

## Modification d'élément (problème)

```rust [3-4|5-6|]
fn main() { // Prob: on veut modifier v[1]
    let v = vec![1, 2, 3, 4];
    let mut elem = v[1]; // que se passe-t-il à votre avis?
    println!("elem = {}", elem);
    elem += 1;
    println!("elem = {}, v = {:?}", elem, v); // elem/v[1] vaut?
}
```

## Modification d'élément (version 2)

```rust compile_fail [3]
fn main() {
    let v = vec![1, 2, 3, 4];
    let mut elem = &v[1]; // que se passe-t-il à votre avis?
    *elem += 1;
    println!("elem = {}, v = {:?}", elem, v);
}
```

## Modification d'élément (version 3)

```rust compile_fail [2,3|6|]
fn main() {
    let v = vec![1, 2, 3, 4];
    let mut elem = &mut v[1]; // que se passe-t-il à votre avis?
    *elem += 1;
    println!("elem = {}, v = {:?}", elem, v);
}
```

## Modification d'élément (version 4)

```rust compile_fail [3-6|7|]
fn main() {
    let mut v = vec![1, 2, 3, 4];
    let mut elem = &mut v[1]; // que se passe-t-il à votre avis?
    *elem += 1;
    println!("elem = {}, v = {:?}", elem, v);
}
```

## Modification d'élément (version 5)

```rust [3-6|7|]
fn main() {
    let mut v = vec![1, 2, 3, 4];
    let mut elem = &mut v[1]; // que se passe-t-il à votre avis?
    *elem += 1;
    print!("elem = {elem}");
    println!(", v = {:?}", v);
}
```

## Lecture d'éléments (1/3)

```rust compile_fail
struct Int(i32);
fn main() {
    let v = vec![Int(1), Int(2), Int(3), Int(4)];
    let _elem =  v[1]; // que se passe-t-il à votre avis?
    let _elem = &v[1]; // que se passe-t-il à votre avis?
}
```

## Lecture d'éléments (2/3)

```rust
fn main() {
    let v = vec![1, 2, 3, 4];
    let elem = &v[1]; // retourne une référence sur le premier élément
    println!("elem = {}", elem);
    let elem = v.get(1); // retourne une Option sur une référence sur le premier élément
    println!("elem = {:?}", elem);
    // let elem = &v[100]; // retourne une référence sur le 100e élément, hmmm.... panique
    // println!("elem = {}", elem);
    // let elem = v.get(100); // retourne une Option sur une référence sur le 100e élément
    // println!("elem = {:?}", elem);
}
```

## Lecture d'éléments (3/3)

```rust
fn main() {
    let mut v = vec![1, 2, 3, 4];
    v.push(-1);
    println!("v = {:?}", v);
    let elem = &v[1]; // retourne une Option sur une référence sur le premier élément
    println!("elem = {:?}", elem);
    v.push(10);
    // que vaut v ici?
    println!("v = {:?}", v);
}
```

## Itérer sur un `Vec` (borrow)

```rust
fn main() {
    let v = vec![1, 2, 3, 4];
    for i in &v {
        println!("i = {}", i);
    }
    let mut v = vec![1, 2, 3, 4];
    for i in &mut v {
        *i += 20;
    }
    println!("v = {:?}", v);
}
```


## Itérer sur un `Vec` (move)

```rust compile_fail
#[derive(Debug)]
struct Int(i32);
fn main() {
    let v = vec![Int(1), Int(2), Int(3), Int(4)];
    for i in v {
        println!("i = {:?}", i);
    }
    println!("v = {:?}", v);
}
```

