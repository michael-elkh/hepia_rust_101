# Les types

## Le système de type en Rust

- Langage **statiquement et fortement typé**: 
    - types connus **à la compilation**,
    - conversion entre types **explicite**.
- Les types sont **inférés** quand cela est possible.
- Il est parfois nécessaire d'annoter le type explicitement.

```rust
fn main() {
    let x:i32 = 5; // entier 32 bits dont le type est annoté
    let y = 5; // entier 32 bits dont le type est inféré
    println!("Ces deux lignes sont équivalentes x={} et y={}", x, y); // affichage
}
```

## Les types de base

- Il existe plusieurs types de base:
    - Les entiers.
    - Les nombres à virgule flottante.
    - Les booléens.
    - Les caractères.

# Les types numériques

## Les entiers existants

- Il existe 5 tailles d'entiers (8, 16, 32, 64, et 128 bits) qui viennent en *signé* ou *non-signé*:
    - `i8`, `u8`, `i16`, `u16`, ...
- Également en taille dépendant de l'architecture
    - `isize` entier signé 32 ou 64 bits.
    - `usize` entier non-signé 32 ou 64 bits.

## Les entiers (particularités)

- Par défaut un entier est de type `i32`.
- Les types `isize` et `usize` sont utilisés typiquement pour indexer des collections.
- On peut spécifier le type d'un entier:
	- `42u8` est un entier 8 bits non signé.
	- `42isize` est un entier de 32 ou 64 bits signé.
- Pour améliorer la lisibilité on peut également ajout des "_":
	- On peut écrire `1000000` comme `1_000_000`

## Les entiers (conversions)

- La conversion implicite **n'existe pas**.

```rust compile_fail [2|]
fn main() {
    let x:i64 = 5i32; // entier 32 bits dans une variable 64 bits
}
```

- Entre entiers on peut utiliser `as`.

```rust [2|]
fn main() {
    let x:i64 = 5i32 as i64; // entier 32 bits dans une variable 64 bits
    println!("Conversion de 32 en 64 bits pour x = {}.", x);
}
```

## Les entiers (3/3)

- Le dépassement de capacité est **interdit**.
```rust compile_fail [4|]
fn main() {
    let x:i32 = 12345678; 
    let y:i32 = 12345678; 
    println!("L'entier x vaut {}.", x * y);
}
```


## Les nombres à virgule flottante

- Deux tailles de nombres à virgule flottante (32 et 64 bits): `f32`,`f64`.
- Par défaut un nombre à virgule flottante est un `f64`

## Les nombres à virgule flottante (conversion)

- On ne **peut pas** convertir implicitement d'un type à un autre.

```rust compile_fail [2|]
fn main() {
    let x:f64 = 3.14159265359f32; // flottant 32 bits en flottant 64 bits
}
```

- On peut convertir avec `as`.

```rust
fn main() {
    let x = 3.14159265359f64 as f32; // flottant 64 bits en flottant 32 bits
    println!("Conversion de pi en 32 bits, x = {}.", x); // affichage
    let y = 3.14159265359f64 as isize; // flottant 64 bits en entier 
    println!("Conversion de pi en entier 64 bits, y = {}.", y); // affichage
}
```

## Les opérations arithmétiques

- Toutes les opérations arithmétiques standard: `+, -, *, /, %`

```rust [2,4,6,8|]
fn main() {
    let addition = 3 + 8;
    println!("La valeur de l'addition est: {}", addition);
    let soustraction = 3.5 - 8.1;
    println!("La valeur de la soustraction est: {}", soustraction);
    let multiplication = -3.33 * 3.33;
    println!("La valeur de la multiplication est: {}", multiplication);
    let division = 7 / 2; // division entière
    println!("La valeur de la division est: {}", division);
}
```

## Assignations

- Opérations arithmétiques et assignations: `+=, -=, *=, /=, %=`.

```rust [2-3,5-6|]
fn main() {
    let mut addition = 3;
    addition += 5;
    println!("La valeur de l'addition est: {}", addition);
    let mut multiplication = 3.5;
    multiplication *= - 8.1;
    println!("La valeur de la multiplication est: {}", multiplication);
}
```

# Les types booléens

## Les booléens

- Une variable booléenne est de type `bool`.
- Elle peut prendre deux valeurs: `true`, `false`
- Peut se convertir en entier (l'inverse est faux).

```rust [2,4,6,7|]
fn main() {
    let x = true; // Booléen inféré vrai
    println!("La variable x est {}.", x); // affichage
    let x: bool = false; // Booléen faux
    println!("La variable x est {}.", x); // affichage
    let x = false as i32; // Booléen faux
    let y = true as i32; // Booléen faux
    println!("False est {}, et True est {}.", x, y); // affichage
}
```

## Les opérations booléennes


- Comparaisons bit à bit: `&` (et), `^` (ou)
- Comparaisons bit à bit et assignations: `&=, ^=`.
- Comparaisons logiques `==, !=, &&, ||, <, >, <=, >=`
```rust
fn main() {
    let x = 3;
    let y = 8;
    println!("{} est-il égal à {}? {}", x, y, x == 8);
    println!("{} est-il différent de {}? {}", x, y, x != 8);
}
```

# Le type caractère

## Les caractères

- Une variable de type **caractère** est notée `char`.
- Elle contient toute valeur **unicode scalaire**, soit environ 137000 caractères (pas seulement du ASCII!).
- Un caractère est toujours mis entre ' '.
- Composants du type `String`.

```rust [2,4|]
fn main() {
    let x = 'a'; // Le caractère a
    println!("La variable x est {}.", x); // affichage
    let chat = '😻';
    println!("La variable chat est {}.", chat); // affichage
}
```

# Les types composés

- On peut construire des types plus complexes (composés) à partir de types de base que nous venons de voir:
    - La type `Struct`
    - Le type `Tuple`
    - Le type `Enum`
    - Le type `Array`
    - Le type `Vec`
    - Le type `String`
    - ... 
    
