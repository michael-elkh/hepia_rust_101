# Introduction

## Avant de commencer, quelques références

* [The Book](https://doc.rust-lang.org/book/)
* [The Book, mais avec des goodies](https://rust-book.cs.brown.edu/)
* [La documentation officielle](https://doc.rust-lang.org/)
* [Rust by Example](https://doc.rust-lang.org/rust-by-example/index.html)
* [Un discord](https://discord.com/invite/rust-lang)

## La communauté Rust

- [Reddit débutants](https://www.reddit.com/r/learnrust/)
- [Reddit](https://www.reddit.com/r/rust/)
- [Le forum des utilisateurs](https://users.rust-lang.org/)
- [Un discord](https://discord.com/invite/rust-lang-community)

## Rust playground

- Pour jouer avec le langage: [le Rust playground](https://play.rust-lang.org/)

## Pourquoi Rust?

- Langage le plus aimé sur stackoverflow en:
    - 2016, 
    - 2017,
    - 2018,
    - 2019, 
    - 2020,
    - 2021,
    - 2022, 
    - 2023,
    - 2024,

## Et en vrai?

- Une mascotte super mignonne

![Ferris](figs/ferris.png)

## Une brève histoire du Rust

- Créé comme projet personnel par Graydon Hoare chez Mozilla en 2006.
- Financé par Mozilla dès 2010.
- Rendu public la première fois en 2012 (version 0.4).
- Orientation vers la programmation système.
- Première version stable (1.0) en 2015 (9 ans cette année).
- Stabilisation de fonctionnalités tous les 6 semaines.
- Version stable actuelle 1.81.0 <!-- TODO UPDATE -->

## Concept de base

- Compilateur très pointilleux.
- Langage moderne:
    - procédural **et** fonctionnel,
    - gestion de dépendances simple,
    - bonne intégration dans les IDE modernes,
    - gestion des erreurs élégante.

## Sûreté

- Fortement typé.
- Pas d'accès mémoire illégal (dépassement de capacité, use after free).
- La désallocation est automatique.
- Les erreurs mémoires sont donc TRÈS difficile à faire.

## Concurrence

- *Concurrency without fear*.
- Le système de type empêche un accès concurrent à des données.
- La synchronisation des données doit être explicitement faite.
- Détection si l'accès asynchrone est sûr à la compilation.
- Protection contre les accès concurrents. 

## Rapidité

- Les conditions de sécurité sont gérées *à la compilation* (pas de coût à l'exécution).
- Abstractions à coût nul (zero cost abstractions).

## God mode

- Le compilateur a des règles très strictes...
- ... mais il est possible de contourner certaines règles.
- Syntaxe spéciale pour autoriser les comportement potentiellement dangereux (`unsafe`).
- Un guide pour bien vivre dangereusement <https://doc.rust-lang.org/nomicon/>.

