# Tableaux statiques

## Type `array`

```rust [2,4|]
fn main() {
    let entiers = [1, 2, 3, 4, 5];
    println!("Les 5 premiers entiers naturels: {:?}.", entiers); 
    let zeros: [i32; 10] = [0; 10]; // déclaration explicite du type
    println!("Dix zéros: {:?}.", zeros); 
}
```

## Problème?

```rust compile_fail
fn main() {
    let entiers = [1, 2.0, 3, 4, 5];
}
```

## Accès aux éléments

```rust [2-3|4-5|]
fn main() {
    let jours = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi",
                   "Samedi", "Dimanche"];
    let premier = jours[0]; // [index]
    let dernier = jours[6]; // index in [0, size-1]
    println!("Le premier jour de la semaine est 
        le {} et le dernier {}.", premier, dernier); 
}
```

## Dépassement de capacité

```rust compile_fail
fn main() {
    let jours = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi",
                   "Samedi", "Dimanche"];
    let _oups = jours[7]; // dépassement de capacité
}
```

## Modification des éléments: erreur

```rust compile_fail
fn main() {
    let entiers = [1, 2, 3, 4, 5];
    entiers[0] = -4; // immutable => erreur
}
```

## Modification des éléments: ok

```rust
fn main() {
    let mut entiers = [1, 2, 3, 4, 5];
    entiers[0] = -4; // immutable => ok
    println!("Les 5 entiers : {:?}.", entiers); 
}
```

# `N`-uplets

## `Tuples` 

- *Collection* ordonnée de `n` objets: `n`-tuplet.
```rust
fn main() {
    let triple = (1.0, 'c', 18usize);                // type inféré
    println!("Un tuple {:?}", triple);
    let tuple: (i32, char)  = (666, 'a'); // type explicite
    println!("Un autre tuple {:?}", tuple);
}
```

## Exemple

```rust [1-3|4-6|7-12|]
fn area(w: usize, h: usize) -> usize {
    w * h
}
fn area_t(r: (usize, usize)) -> usize {
    r.0 * r.1 // sélecteur
}
fn main() {
    let w = 10; 
    let h = 20;
    println!("Area = {}.", area(w,h));
    println!("Area with tuple = {}.", area_t((w,h)));
}
```

## Destructuration

```rust [3|5-7|]
fn main() {
    let tuple = (1.0, 'c', 18usize);
    let (fl, ch, us) = tuple;
    println!("Le tuple destructuré: {}, {}, {}", fl, ch, us);
    let fl_ind = tuple.0;
    let ch_ind = tuple.1;
    let us_ind = tuple.2;
    println!("Le tuple re-destructuré: {}, {}, {}", fl_ind, ch_ind, us_ind); 
}
```

# Structures

## `struct`: initialisation

```rust [1-4|6-8|]
struct Etudiant {
    nom: String,
    actif: bool,
}
fn main() {
    let etu = Etudiant {
        nom: String::from("Jean-Paul Dax"), 
        actif: true,
    };
}
```

## `struct`: sélecteur

```rust [10|]
struct Etudiant {
    nom: String,
    actif: bool,
}
fn main() {
    let etu = Etudiant {
        nom: String::from("Jean-Paul Dax"), 
        actif: true,
    };
    println!("Nom: {}", etu.nom);
}
```


## `struct`: mutabilité

```rust [6|10|]
struct Etudiant {
    nom: String,
    actif: bool,
}
fn main() {
    let mut etu = Etudiant {
        nom: String::from("Jean-Paul Dax"), 
        actif: true,
    };
    etu.actif = false;
}
```

## `tuple struct`

```rust [2|3|]
fn main() {
  struct Point2d(i32, i32);
  let origine = Point2d(0,0);
  println!("L'origine est le point ({}, {}).", origine.0, origine.1);
}
```

# Types énumérés

## `Enum`: déclaration / instantiation

```rust [1-5|7-9|]
enum TypeEnum {
    Id1,
    Id2,
    Id3,
}
fn main() {
    let ins1 = TypeEnum::Id1;
    let ins2 = TypeEnum::Id2;
    let ins3 = TypeEnum::Id3;
}
```

- **Question**: Quel est le type de `ins1`, `ins2`, et `ins3`?

## `Enum`: exemple

```rust [7-8|5|]
enum TypeEnum {
    Id1,
    Id2,
}
fn foo(enum_type: TypeEnum) { }
fn main() {
    foo(TypeEnum::Id1); // appel valide
    foo(TypeEnum::Id2); // appel également valide
}
```

## `Enum`: types associés

```rust [2-6|8-10|]
struct TypeStruct { }
enum TypeEnum {
    Id1(char, char), // deux char
    Id2{x: usize},   // struct anonyme 
    Id3(TypeStruct), // struct
}
fn main() {
    let ins1 = TypeEnum::Id1('a','b');
    let ins2 = TypeEnum::Id2 {x: 12usize};
    let ins3 = TypeEnum::Id3(TypeStruct { });
}
```

# Pattern matching

## `match`

```rust [9-13|]
enum TypeEnum {
    Id1,
    Id2,
    Id3,
}
fn main() {
    let data = TypeEnum::Id1;
    let num = 
        match data {
            TypeEnum::Id1 => 1,
            TypeEnum::Id2 => 2,
            TypeEnum::Id3 => 3,
        };
    println!("num = {}", num);
}
```

## `match`: bras manquant

```rust compile_fail [8-10|]
enum TypeEnum {
    Id1,
    Id2,
}
fn main() {
    let data = TypeEnum::Id1;
    let num = 
        match data {
            TypeEnum::Id1 => 1,
        };
}
```

## `match`: défaut

```rust [11|9-12|]
enum TypeEnum {
    Id1,
    Id2,
    Id3,
}
fn main() {
    let data = TypeEnum::Id3;
    let num = 
        match data {
            TypeEnum::Id1 => 1,
            _ => 2,
        };
    println!("num = {}", num);
}
```

## `match`: lier une valeur

```rust [4|10|] no_run
enum AnimalMythique {
    Chupacabra,
    Dahu,
    ChevalAile(String),
}
fn pays_dorigine(animal: AnimalMythique) {
    match animal {
        AnimalMythique::Chupacabra => println!("Mexique"),
        AnimalMythique::Dahu => println!("Suisse"),
        AnimalMythique::ChevalAile(pays) => println!("{}", pays),
    };
}
```
