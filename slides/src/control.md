# Les structures de contrôle

## Les branchements conditionnels

```rust [3-5|6-8|9-11|]
fn main() {
    let x = 15; 
    if x < 10 {
        println!("{} est plus petit que 10.", x);
    } 
    else if x >= 10 && x < 20 {
        println!("{x} est plus grand ou égal à 10 et plus petit que 20.");
    } 
    else {
        println!("{x} est plus grand ou égal à 20.");
    }
}
```

## C'est des **expressions**

```rust [2|4-10|4,6,9,10|]
fn main() {
    let x = -1;
    let sign = 
        if x > 0 {
            1 // pas de ;
        } else if x < 0 {
            -1 // pas de ;
        } else {
            0 // pas de ;
        }; // attention au ;
    println!("Le signe de x est {sign}.");
}
```

## La boucle infinie: `loop`

```rust ignore []
fn main() {
    loop {
        println!("En boucle!");
    }
}
```

## Sortie avec `break`

```rust [3,10|8|]
fn main() {
    let mut i = 0;
    let j = loop {
        println!("{}, en boucle!", i);
        i += 1;
        if i == 10 {
            println!("Fin de la boucle!");
            break i; // i est optionnel
        }
    }; // attention au ;
}
```  

## La boucle `while`

```rust [3,6|]
fn main() {
    let mut i = 0;
    while i != 10 {
        println!("{}-ème boucle!", i);
        i += 1;
    }
    println!("Fin de la boucle!");
}
```  

## La boucle `for` et `continue`

```rust [2,7|3-5|]
fn main() {
    for i in 0..10 {
        if i % 2 == 0 {
           continue;
        }
        println!("{}-ème boucle!", i);
    }
    println!("Fin de la boucle!");
}
```  
- `0..10` de `0` à `10` non inclus.
- `0..10` est une `Range` (une collection).


