# Commentaires

## Commentaires simples

```rust [2-3|4-7|]
fn main() { 
	// Tout programme Rust a un point d'entrée qui est
	// une fonction fn main().
	/* 
	On peut également documenter sur plusieurs lignes. 
	Cela se fait comme ceci.
	*/ 
    println!("Hello, World!"); // Ceci est un macro en ligne.
}
```

## Commentaires de documentation

- La commande `cargo doc --open` génère la documentation en HTML.

```rust [1-2|4|6-9|]
/// Cette fonction ajoute deux, puis multiplie par trois.
/// Cette documentation supporte le Markdown.
///
/// # Exemple
///
/// ```
/// let five = 5;
/// add_two_mul_three(five);
/// ```
pub fn add_two_mul_three(x: i32) -> i32 {
    (x + 2) * 3
}
```

## Tests de documentation

```rust [6-10|]
/// Cette fonction ajoute deux, puis multiplie par trois.
/// Elle doit se trouver dans la librairie (pas dans le `main.rs`).
///
/// # Exemple
///
/// ```
/// use tests::add_two_mul_three;
/// let five = 5;
/// add_two_mul_three(five);
/// ```
pub fn add_two_mul_three(x: i32) -> i32 {
    (x + 2) * 3
}
```

## Tests de documentation: compte rendu

```sh
$ cargo test --doc
    Finished dev [unoptimized + debuginfo] target(s) in 0.01s
   Doc-tests hello_world
running 1 test
test src/lib.rs - add_two_mul_three (line 11) ... ok
test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
```

## Commentaires de module

- La documentation du projet en entier se fait avec `//!` en début de ligne.

```rust [1-5|]
//! # Hello world
//! 
//! `hello_world` est l'exemple typique de tout
//! cours de programmation. Ces commentaires vont en entête
//! d'un fichier de module.

/// Cette fonction ajoute deux, puis multiplie par trois.
pub fn add_two_mul_three(x: i32) -> i32 {
    (x + 2) * 3
}
```
