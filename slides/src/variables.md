# Les variables

## Lien entre une valeur **typées** et un symbole

```rust [2|3-4|5|]
fn main() {
    let x = 5; // entier 32 bits (inféré)
    let y;
    y = "5 est liée avec la variable x"; // Chaîne de caractères (inféré)
    let z: f64 = 1.0; // float 64bits (explicite)
}
```

## Inférence de type

```rust
fn main() {
    let x = 5;
    let y = 10;
    let z = x + y; // Quel type?
}
```

## Nommage: mots-clés interdits

```rust compile_fail
fn main() {
    let if = 7;
    let while = 'a';
}
```

## Interdiction: utilisation sans déclaration

```rust compile_fail
fn main() {
    println!("La valeur de x est: {}", x);
}
```

## Interdiction: utilisation sans initialisation

```rust compile_fail [2-3|]
fn main() {
    let x: i32;
    println!("La valeur de x est: {}", x);
}
```

## Portée: ok

```rust [2|3-5|6|]
fn main() {
    let x = 5; // début de protée de x
    { // ouverture de bloc
        println!("La valeur de x est: {}", x);
    } // fermeture
} // fin portée de x
```

## Portée: pas ok

```rust compile_fail [2-5|6|]
fn main() {
    {
        let x = 5; 
        println!("La valeur de x est: {}", x);
    } // fin de la portée de x
    println!("La valeur de x est: {}", x);
}
```
- À la fin du bloc la variable est détruite.

## Portée: masquage

- La re-déclaration d'une variable dans un bloc intérieur **masque** la variable extérieure.

```rust [2|3-6|]
fn main() {
    let x = 1000; 
    {
        let x = 5; 
        println!("La valeur de x est: {}", x);
    }
    println!("La valeur de x est: {}", x);
}
```

## Mutabilité/**Immutabilité**

```rust compile_fail [2|3|]
fn main() {
    let x = 5; // variable immutable
    x = 6;     // interdit
}
```

## **Mutiabilité**/Immutabilité

```rust [2-3|4-5|]
fn main() {
    let mut x = 5; // variable mutable
    x = 6;         // autorisé
}
```

## Constantes

```rust [1|]
const PI: f64 = 3.14159265359;
fn main() {
    println!("La valeur de pi est: {}", PI);
}
```

## Quelles différences avec les variables

- Plusieurs différences notables:
	1. On ne peut pas utiliser `mut`
    2. On doit explicitement déclarer le *type* d'une constante.
    3. La valeur d'une constante doit être déterminée à la compilation.
    4. Leur identifiant est en majuscules (convention).

## Masquage (Shadowing)

```rust
fn main() {
    let x = 5;
    let x = x + 1;
    println!("La valeur de x est: {}", x);
}
```
- La nouvelle déclaration **masque** la précédente.
- Pratique quand on veut initialiser une variable de façon compliquée sans variables qui traînent à la fin.


## Masquage: remarque

- La nouvelle variable n'a pas forcément le même type que la variable d'origine.

```rust
fn main() {
    let x = "La variable d'origine";
    println!("La valeur de x est: {}", x);
    let x = x.len();
    println!("La valeur de x est: {}", x);
}
```

