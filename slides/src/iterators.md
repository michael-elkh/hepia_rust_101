# Les itérateurs

## Le trait itérateur

Le trait itérateur d'après la documentation

```rust
trait Iterator {
    type Item;
    fn next(&mut self) -> Option<Self::Item>;
}
```

## Les itérateurs infinis

```rust[7-11|13-15]
const fn my_random(s: i32) -> i32 {
    // PRNG See wikipedia : https://en.wikipedia.org/wiki/Linear_congruential_generator
    (((1664525i64 * s as i64) + 1013904223i64) % (1i64 << 32)) as i32
}

fn main() {
    let mut seed: i32 = 123456;
    let mut rand_iter = std::iter::from_fn(move || {
        seed = my_random(seed);
        Some(seed % 100)
    });

    println! {"{:?}", rand_iter.next()};
    println! {"{:?}", rand_iter.next()};
    println! {"{:?}", rand_iter.next()};
}
```

## La fonction iter

```rust[3-4|6-9]
fn main()
{
    let v = vec![0, 1, 2];
    let mut v_iter = v.iter();

    println! {"{:?}", v_iter.next()};
    println! {"{:?}", v_iter.next()};
    println! {"{:?}", v_iter.next()};
    println! {"{:?}", v_iter.next()};
}
```

## La fonction collect

```rust[7-11|13|14]
const fn my_random(s: i32) -> i32 {
    // PRNG See wikipedia : https://en.wikipedia.org/wiki/Linear_congruential_generator
    (((1664525i64 * s as i64) + 1013904223i64) % (1i64 << 32)) as i32
}

fn main() {
    let mut seed: i32 = 123456;
    let mut rand_iter = std::iter::from_fn(move || {
        seed = my_random(seed);
        Some(seed % 100)
    });

    let v_rand: Vec<i32> = rand_iter.take(5).collect();
    println!("{:?}", v_rand);
}
```
- La fonction `take(5)` crée un nouvel itérateur de 5 éléments. 

## `map()`

```rust[5|]
fn main()
{
    let v = vec![0, 1, 2];
    let v_res: Vec<i32> = v.iter()
                           .map(|x| x + 1)
                           .collect();

    println! {"{:?}", v_res};
}
```

## `filter()`

```rust[6|]
fn main()
{
    let v = vec![0, 1, 2];
    let v_res: Vec<i32> = v.iter()
                           .map(|x| x + 1)
                           .filter(|x| x % 2 == 1)
                           .collect();

    println! {"{:?}", v_res};
}
```

## Rappel élément neutre

L'élément neutre `N` d'une opération `★` est l'élément tel que `N ★ x = x ★ N = x` pour tout `x`.

- Exemple : 0 pour l'addition, car `0 + x = x + 0 = x`
- Exemple : 1 pour la multiplication, car `1 * x = x * 1 = x`

## `fold()`

```rust[8|4|]
fn main()
{
    let v = vec![0, 1, 2];
    let sum: i32 = v
        .iter()
        .map(|x| x + 1)
        .filter(|x| x % 2 == 1)
        .fold(0, |acc, current| acc + current);

    println! {"{:?}", sum};
}
```

## `zip()`

```rust[6-8|9-11|]
fn main()
{
    let chiffres = vec![1, 2, 3, 4];
    let lettres = vec!['a', 'b', 'c', 'd'];

    chiffres
        .iter()
        .zip(lettres.iter())
        .for_each(
            |(chiffre, lettre)| println!("{lettre} est la lettre n°{chiffre} de l'alphabet",),
        );
}
```

## Lazy evaluation et performances

- La performance des itérateurs est comparable aux boucles
- Les transformations sont appliquées uniquement lorsque que l'on veut récupérer une valeur. On appelle ça **lazy evaluation**
