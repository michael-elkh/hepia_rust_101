# Rust-101

Le but de ce projet est de créer un cours de 5 séances de 2h de théorie avec également 5 fois 2h d'exercices.
L'objectif du cours est de se familiariser avec le langage de programmation Rust tout en faisant une
introduction à différents concepts avancés de programmation orientée objet et de programmation fonctionnelle.

Le dbook et les slides du cours se trouvent sur les liens:

- [Book](https://malaspinas.academy/rust-101/book/)
- [SLides](https://malaspinas.academy/rust-101/slides/)

## Contexte

Les élèves ont suivi un cours de `C` pendant un an et ont donc des notions de base sur les structures de contrôle,
la compilation, les types, etc, ainsi que des notions algorithmiques de base.

Les élèves recevront du matériel sur une introduction aux bases, très basiques, du Rust à étudier une semaine avant le début du cours qui sera évalué avec un QCM de 15min au début de la première séance.

## Prérequis

### Installation de Rust

Pour compiler du code rust, il faut installer la chaîne de compilation et le compilateur `rustc` à l'aide de l'outil [Rustup](https://rustup.rs/).

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

### Plugin pour `codium`

Il existe un très bon plugin recommandé pour `codium` ou `vscode`: [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer).
Nous vous recommandons de l'installer pour avoir la coloration syntaxique et l'analyse des erreurs.

### Compiler le cours

Après avoir installé la chaîne de compilation, vous pouvez également générer les résumés se trouvant dans le répertoire `book` et `slides`
du repo git que vous pouvez cloner à l'aide de la commande suivante
```bash
git clone https://gitedu.hesge.ch/orestis.malaspin/rust-101
```
Pour ce faire il faut installer plusieurs programmes:

* [mdbook](https://rust-lang.github.io/mdBook/) pour le livre et les slides,
* [modbook-linkcheck](https://github.com/Michael-F-Bryan/mdbook-linkcheck) pour la vérification des liens su livre,
* [mdslides](https://github.com/ferrous-systems/mdslides/), uniquement pour les slides.
* [mdbook-pdf](https://github.com/HollowMan6/mdbook-pdf), pour générer le cours au format pdf.

### Installation de `mdbook`

Pour installer `mdbook` depuis la dernière version de <https://crates.io/>
```bash
cargo install mdbook
```

### Installation de `mdbook-linkcheck`

Pour installer `mdbook-linkcheck` depuis la dernière version de <https://crates.io/>
```bash
cargo install mdbook-linkcheck
```
Il est dès lors possible de générer le livre contenant les résumés des codes discutés en classe
en allant dans le répertoire du cours (que vous aurez préalablement cloné).
```bash
cd book
mdbook build
```
L'index du livre se trouve ensuite dans `book/html/index.html`.

### Installation de `mdslides`

Pour installer `mdslides`, il faut télécharger l'archive correspondant à vos besoins sur
[github](https://github.com/ferrous-systems/mdslides/releases), puis d'en extraire l'exécutable
dans votre `PATH`. Si vous êtes sous Linux vous pouvez faire les commandes suivantes
```bash
$ curl -sSL https://github.com/ferrous-systems/mdslides/releases/download/v0.3.0/mdslides-v0.3.0-x86_64-unknown-linux-gnu.tar.xz \
    | tar -xJ "mdslides-v0.3.0-x86_64-unknown-linux-gnu/mdslides" \
    && mv mdslides-v0.3.0-x86_64-unknown-linux-gnu/mdslides $HOME/.cargo/bin/ \
    && rm -r mdslides-v0.3.0-x86_64-unknown-linux-gnu
```
qui installeront `mdslides` dans le répertoire `$HOME/.cargo/bin`.
Il est dès lors possible de générer les slides du cours en allant dans le répertoire du cours (que vous aurez préalablement cloné).
Pour ce faire exécuter les commandes
```bash
$ cd slides
$ ./build_slides.sh 
```
L'index des slides se trouve ensuite dans `slides/index.html`.

### Installation de `mdbook-pdf`

Pour installer `mdbook-pdf` depuis la dernière version de <https://crates.io/>
```bash
cargo install mdbook-pdf
```

`mdbook-pdf` nécessite un navigateur de type chromium (Google Chrome / Chromium / Microsoft Edge) installé.

## Inspiration

L'inspiration de ce cours est tirée de trois cours qui ont l'air très bons:

1. Rust 101: an open-source university course <https://tweedegolf.nl/en/blog/80/rust-101-open-source-university-course> (cours de base avec introduction au langage).
2. Rust-101 de Ralf Jung <https://www.ralfj.de/projects/rust-101/main.html> (cours plutôt avancé avec beaucoup de notions de programmation fonctionnelle).
3. Le cours Rust de Ferrous Systems <https://github.com/ferrous-systems/rust-training>.

## Matériel

Le matériel de cours sera composé de trois composants:

1. Slides avec la théorie et des exemples (dans le répertoire `slides`).
2. Codes source exécutable avec des exemples (dans le répertoire `codes`).
3. Discussion des codes (dans le répertoire `book`).
4. Une liste d'exercices (dans le répertoire `exercices`) un peu plus "complexes" que les Rustlings (voir ci-dessous).

## Rustlings

Afin de vous habituer à la syntaxe de Rust, nous vous proposerons tout au long du cours de faire
les exercices de [Rustlings](https://github.com/rust-lang/rustlings) que nous avons traduits en français.

Pour installer la version anglaise référez vous au [guide d'installation](https://github.com/rust-lang/rustlings#macoslinux) de Rustlings.
Pour la version française c'est un peu plus compliqué, car Rustlings n'a pas vraiment été prévu pour être traduit (qui sait 
[un jour peut-être](https://github.com/rust-lang/rustlings/issues/1627)).

Pour commencer il faut cloner le [repo suivant](https://gitedu.hesge.ch/orestis.malaspin/rustlings-french)
```bash
$ git clone https://gitedu.hesge.ch/orestis.malaspin/rustlings-french
```
Aller dans le répertoire nouvellement créé
```bash
$ cd rustlings-french
```
Puis installer tout ce qu'il y a à installer
```bash
$ cargo install --path .
```
Pour tester si l'installation a fonctionné vous pouvez exécuter
```bash
$ rustlings
```
Vous devriez voir
```console

       bienvenue à...
                 _   _ _
  _ __ _   _ ___| |_| (_)_ __   __ _ ___
 | '__| | | / __| __| | | '_ \ / _` / __|
 | |  | |_| \__ \ |_| | | | | | (_| \__ \
 |_|   \__,_|___/\__|_|_|_| |_|\__, |___/
                               |___/

Merci d'avoir installé Rustlings !

C'est votre première fois ? Ne vous inquiétez pas, Rustlings a été conçu pour les débutants ! Nous allons
allons vous apprendre beaucoup de choses sur Rust, 
mais avant de commencer, voici quelques remarques sur le fonctionnement de Rustlings :

1. Le concept central de Rustlings est que vous résolvez des exercices. Ces
   exercices contiennent généralement une erreur de syntaxe, ce qui les fait
   de les faire échouer à la compilation ou au test. Parfois, il y a une erreur de logique
   au lieu d'une erreur de syntaxe. Quelle que soit l'erreur, c'est à vous de la trouver et de la corriger !
   Vous saurez que vous l'avez corrigée car l'exercice compilera et
   Rustlings pourra passer à l'exercice suivant.
2. Si vous lancez Rustlings en mode "watch" (ce que nous recommandons), il commencera automatiquement
   automatiquement par le premier exercice. Ne vous laissez pas déconcerter par un message 
   d'erreur qui apparaît dès que vous lancez Rustlings ! Il s'agit d'une partie de l'exercice que vous devez résoudre
   alors ouvrez le fichier de l'exercice dans un éditeur et commencez votre travail de détective !
   travail de détective !
3. Si vous êtes bloqués sur un exercice, il y a un indice que vous pouvez consulter en tapant
   'hint' (en mode veille), ou en exécutant `rustlings hint nom_de_l_exercice`.
4. Si un exercice n'a pas de sens pour vous, n'hésitez pas à ouvrir une issue sur GitHub !
   (https://github.com/rust-lang/rustlings/issues/new). Nous examinons chaque issue,
   et parfois, d'autres apprenants le font aussi, donc vous pouvez vous aider mutuellement !
5. Si vous voulez utiliser `rust-analyzer` avec les exercices, qui fournit des fonctionnalités comme
   l'autocomplétion, lancez la commande `rustlings lsp`.

Vous avez compris tout cela ? C'est parfait ! Pour commencer, lancez la commande `rustlings watch` afin d'obtenir le premier exercice
exercice. Assurez-vous que votre éditeur est ouvert !
```

## Syllabus

L'idée principale du cours est de baser toutes les séances sur un exemple très simple de calcul du minimum d'une liste.
Cela permet d'introduire tous les concepts du Rust (struct, enum, ownership, pattern matching, itérateurs, etc.) avec une application
simple que tout le monde peut comprendre. On pourra ainsi comparer l'implémentation en C et en Rust de 

Le cours théorique est découpé comme suit:

0. Installation, Hello World.
1. Introduction à la syntaxe (structures de contrôle et types de base aka tous ceux qui sont Copy).
2. Types avancés (enum) et pattern matching.
3. Généricité et traits (Clone, Copy p.ex.).
4. Ownership, Borrowing.
5. Modules et visibilité.
6. Tests, documentation, outils variés (rustfmt, clippy, etc).
7. Gestion d'erreurs (Option, Result).
8. Closures (Fonctions anonymes)
9. Collections (Vec, String)
10. Itérateurs
11. Smart pointeurs.
12. CLI, I/O.
13. Unsafe Rust.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.
