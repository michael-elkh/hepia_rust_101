# Summary

[Introduction](introduction.md)

- [Les bases du Rust 1](./bases1.md)
- [Les bases du Rust 2](./bases2.md)
- [Les types avancés](./types_avances.md)
- [La généricité et des types composés](./gen_types_composes.md)
- [La propriété](./propriete.md)
- [Modules et visibilité](./modules_visibilite.md)
- [Les petits trucs sympas qui aident au développement](./tooling.md)
- [La gestion des erreurs en Rust](./gestion_erreurs.md)
- [Les closures](./closures.md)
- [Les itérateurs](./iterateurs.md)
- [Les collections](./collections.md)
- [Lifetimes](./lifetimes.md)
- [CLI](./cli.md)
- [Unsafe](./unsafe.md)
