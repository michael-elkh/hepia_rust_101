# Introduction

## Crédits et licence

Ce cours est fortement inspiré de l'excellent tutoriel *Rust-101* écrit par Ralf Jung <https://www.ralfj.de/projects/rust-101/main.html>. Il est publié sous la license [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).

## Préambule

Ce court texte n'a pas vocation à remplacer un cours complet, mais à rappeler les concepts importants en les illustrant
à l'aide de codes courts.

Les codes discutés ont tous pour but de calculer la valeur minimale d'entiers contenus dans une liste. La difficulté
et l'élégance de ces codes ira en augmentant pour illustrer de façon itératives les différents concepts du présents
dans le langage.

### Installation du compilateur Rust

Pour pratiquer le Rust, il est nécessaire d'installer le compilateur Rust. Il n'est pas recommandé d'utiliser votre gestionnaire de paquet,
mais plutôt de télécharger toute la chaîne de compilation grâce à l'outil [rustup](https://www.rust-lang.org/tools/install).
Ou alors d'exécuter la commande suivante dans un terminal
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
Ce script installera pour vous le gestionnaire de paquet et de compilation `cargo`,
le compilateur `rustc`, ainsi que le linter `clippy` et l'outil de formatage de code
`rustfmt`.

Vous pouvez maintenant créer un nouveau projet rust (astucieusement nommé `new_project`) avec la commande
```bash
cargo new new_project
```
Cette commande crée un répertoire `new_project`, ainsi que les fichiers `new_project/Cargo.toml` et `new_project/main.rs`.

Vous pouvez compiler votre programme avec 
```bash
cargo build
```
Puis l'exécuter à l'aide de la commande
```bash
cargo run
```

La commande cargo run dépend de l'étape de compilation, par conséquent si le code n'est pas compilé, alors la commande `cargo run` lancera la compilation avant d'exécuter votre programme.

Il est également possible de nettoyer les artéfacts de compilation ainsi que l'exécutable à l'aide de la commande

```bash
cargo clean
```

## Génération

Il est possible de générer ce cours. Pour ce faire, vous pouvez télécharger les sources depuis le repo du
cours [rust-101](https://gitedu.hesge.ch/orestis.malaspin/rust-101). Puis il faut installer le programme [mdbook](https://rust-lang.github.io/mdBook/) (voir [ce lien](https://rust-lang.github.io/mdBook/guide/installation.html) pour l'installation).

Depuis la racine du repo, il suffit d'exécuter les commandes
```bash
$ cd book
$ mdbook build
```
et vous retrouverez l'index dans le fichier
```bash
$ book/index.html
```

## Références

Il existe un grand nombre de références pour le Rust. Vous en trouverez quelques-unes ci-dessous.

- [The book](https://doc.rust-lang.org/book/). Il s'agit de l'excellent de référence sur le Rust.
- [Rust by example](https://doc.rust-lang.org/rust-by-example/). Une série d'exemple illustrant la syntaxe du langage et ses concepts.
- [Gentle introduction to Rust](https://stevedonovan.github.io/rust-gentle-intro/). Une introduction au Rust pour les personnes connaissant divers autres langages.
- [Rustlings](https://github.com/rust-lang/rustlings). De courts exercices sur le langage Rust.
