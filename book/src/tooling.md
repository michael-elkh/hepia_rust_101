# Discussion du code `tooling`

## Concepts

Les concepts abordés dans cet exemple sont:

1. [La documentation.](#la-documentation)
2. [Les tests.](#les-tests)
3. [Les outils en plus du compilateur](#les-outils-en-plus-du-compilateur)

## Discussion

Le Rust étant un langage moderne, il vient avec tout un tas de features qui sont très appréciables pour écrire du code robuste, propre et réutilisable. On va voir quelle est la syntaxe nécessaire pour écrire documentation et tests.

### La documentation

Il y a différents moyens de documenter un code. Pour un guide bien plus complet que ce qui est résumé ici, vous pouvez vous référer à [ce site](https://doc.rust-lang.org/rustdoc/how-to-write-documentation.html). 

#### Les commentaires

Le plus simple est d'y ajouter des commentaires. En Rust, tous caractères qui suivent des `//` sur la même ligne sont considérés comme un commentaire (ignorés par le compilateur)

```rust,no_run
// Ceci est un commentaire
// Et ceci un autre
let a = 2; // Ici on assigne 2 à a
```

On peut écrire également des commentaires sur plusieurs lignes sans avoir à mettre des `//` sur chacune. Pour ce faire on utilise la syntaxe `/* ... */`

```rust,no_run
/* 
 Ceci est un commentaire
 Et ceci un autre
 Et en voici un dernier
*/
let a = 2; /* Ici on assigne 2 à a */
```

Ce type de documentation se prête très bien à des commentaires sur les détails du code, mais n'est pas très adapté à écrire une documentation plus générale sur le comportement du code. Ainsi, on a autre type de commentaires, qui seront utilisés pour générer automatiquement de la documentation à l'aide de l'outil `rustdoc`.

#### La documentation par composants

La documentation d'un composant, que ça soit une `struct`, un `enum`, une fonction, etc. se fait en préfixant `///` devant la ligne de documentation et la plaçant 
directement au dessus du composant à commenter. Ainsi les exemples suivants permettent de:

* Documenter une constante

```rust,ignore
{{#include ../../codes/rust_lang/tooling/src/lib.rs:size}}
```

* Documenter une fonction

```rust,ignore
{{#include ../../codes/rust_lang/tooling/src/io.rs:function}}
```

* Documenter une fonction statique

```rust,ignore
{{#include ../../codes/rust_lang/tooling/src/something_or_nothing.rs:static_function}}
```

* Documenter un trait

```rust,ignore
{{#include ../../codes/rust_lang/tooling/src/minimum.rs:minimum}}
```

* Documenter un type énuméré et ses variantes

```rust,ignore
{{#include ../../codes/rust_lang/tooling/src/something_or_nothing.rs:something_or_nothing}}
```

#### La documentation d'une `crate` et des modules

Une librairie est appelée `crate` Rust. Afin de donner des informations de haut niveau
sur le fonctionnement d'une librairie, on peut utiliser une syntaxe spéciale `//!` à mettre au début de chaque
ligne de documentation (on peut également utiliser la syntaxe `/*! ... */`), comme ci-dessous

```rust,ignore
{{#include ../../codes/rust_lang/tooling/src/lib.rs:crate}}
```

Cette documentation se met dans le fichier `lib.rs` qui est également l'endroit où on importe les différents modules.

On peut également documenter les modules individuellement. Pour ce faire, il faut utiliser la même syntaxe que pour la documentation de la crate, mais mettre cette documentation au début du fichier contenant chaque module comme par exemple au début du fichier `io.rs`

```rust,ignore
{{#include ../../codes/rust_lang/tooling/src/io.rs:io_module}}
```


#### Markdown

La documentation supporte la syntaxe du [Common Markdown](https://commonmark.org/) comme on peut le voir dans le code ci-dessus. On a en particulier
la possibilité de mettre des titres avec des `#` ou du code avec des code fences. Il est également possible de mettre des liens vers
d'autres parties de la documentation (avec les annotations tu type `[MyStruct]`) ce qui fait de la syntaxe un outil très puissant et intégré fortement au processus de développement.

#### Génération de la documentation

Tout ce travail d'annotation du code source permet d'utiliser `rustdoc` qui est un outil puissant de génération de documentation
sous la forme principal d'un site web. Dans le répertoire où se trouve le fichier `Cargo.toml`, on peut exécuter la commande
```bash
cargo doc
```
et cela va générer la documentation dans un sous répertoire du projet. On peut également automatiquement ouvrir la 
documentation dans un navigateur à l'aide de la commande
```bash
cargo doc --open
```

### Les tests

La documentation aide grandement à la (ré-)utilisation d'une librairie et à son développement (collaboratif ou non).
Durant le processus de développement, il est également très utile (et important) d'écrire des tests pour le code.
Rust propose un framework de tests totalement intégré au langage. On peut ainsi très facilement écrire
des fonctions de test en ajoutant l'annotation `#[test]` directement au dessus de n'importe quelle fonction

```rust,ignore
{{#include ../../codes/rust_lang/tooling/src/lib.rs:test_creation}}
```

La fonction `test_creation()` sera automatiquement appelée lors de l'appel à la commande
```bash
cargo test
```
Aucune fonction non annotée par `#[test]` n'est appelée quand on exécute `cargo test`.
Si la fonction se termine sans erreur, le test est réussi (il est échoué si la fonction
s'arrête en cours de route).
On voit dans la fonction `test_creation()` l'appel à la macro `assert!()` qui prend en argument
une expression booléenne, ici `n1 == SomethingOrNothing::Nothing` dans le premier appel.
Si l'expression prend la valeur `true`, le code continue normalement son exécution, sinon
il fait appel à la macro `panic!()!` et l'exécution est interrompue et un code d'erreur est retourné par le programme.

Dans l'appel à `n1 == SomethingOrNothing::Nothing`, on constate qu'on a besoin de vérifier
si la valeur `n1` est égale à `SomethingOrNothing::Nothing`. L'opérateur `==` n'est pas implémenté
pour les types complexes, ainsi le code suivant ne compile pas
```rust,compile_fail
enum SomethingOrNothing<T> {
    Nothing,
    Something(T),
}
fn main() {
    let n1 = SomethingOrNothing::<i32>::Nothing;
    let b = n1 == SomethingOrNothing::<i32>::Nothing;
}
```
et nous donne un message d'erreur incluant
```bash
note: an implementation of `PartialEq<_>` might be missing for `SomethingOrNothing<i32>`
```
Ainsi, on doit implémenter le trait `PartialEq` pour le type `SomethingOrNothing<T>`,
qui permet de tester l'égalité entre deux instances d'un type
```rust
{{#include ../../codes/rust_lang/tooling/src/something_or_nothing.rs:something_or_nothing}}
{{#include ../../codes/rust_lang/tooling/src/something_or_nothing.rs:partial_eq}}
fn main() {
    let n1 = SomethingOrNothing::<i32>::Nothing;
    assert!(n1 == SomethingOrNothing::<i32>::Nothing);
}
```
On constate que dans notre implémentation, il est nécessaire que `T` implémente également
le trait `PartialEq`. Ainsi, deux `Nothing` sont égaux, un `Nothing` et un `Something` sont différents, et seulement quand les deux valeurs encapsulées dans deux `Something` sont égales
alors nous avons égalité.

On peut également vouloir construire des tests qui échouent comme dans l'exemple ci-dessous
```rust,ignore
{{#include ../../codes/rust_lang/tooling/src/lib.rs:should_panic}}
```
où on a annoté le test avec un `#[should_panic]`. Ce test, bien qu'il panique, sera considéré
comme réussi. Il faut néanmoins rester prudent avec ce type de test. Rien ne garantit que la fonction de test a paniqué au moment espéré. Le code pourrait tout à fait paniquer pour une raison autre que celle attendue. Cela est particulièrement vrai si le test est complexe.

Finalement, il y a également la possibilité de regrouper les tests
comme ci-dessous (dans le fichier `lib.rs` dans cet exemple)
```rust,ignore
{{#include ../../codes/rust_lang/tooling/src/lib.rs:cfg_test}}
```
Pour ce faire, il faut créer un module, (ici `mod tests`) et l'annoter avec une configuration spéciale `#[cfg(test)]`. Cela permet de séparer les tests totalement du reste du code et devoir
importer les différentes implémentations.

Il est également possible de répartir les tests dans différents modules comme dans `minimum.rs` par exemple en plus de `lib.rs`
```rust,ignore
{{#include ../../codes/rust_lang/tooling/src/minimum.rs:cfg_test_min}}
```

### Tests de documentation

Une bonne documentation inclut des exemples de code. Mais il n'y a rien de pire que des
exemples faux ou qui ne marchent pas. Ainsi, Rust offre la possibilité de compiler
et exécuter le code inclut dans la documentation: ces tests sont des **tests de documentation**. En effet, le code entre "code fences"
markdown sera compilé et exécuté (à moins qu'il soit annoté `ignore` où il sera pas compilé et `no_run` où il sera pas exécuté). Il y a deux exemples de tests de documentation dans notre code. Le premier concerne l'implémentation du trait
[Default](https://doc.rust-lang.org/std/default/trait.Default.html) pour `SomethingOrNothing` qui permet
de construire une instance par défaut qui sera la variante `Nothing` (voir `something_or_nothing.rs`).
```rust,ignore
{{#include ../../codes/rust_lang/tooling/src/something_or_nothing.rs:default}}
```
On voit que pour que le test puisse compiler et s'exécuter il est nécessaire d'importer les bons modules/fonctions. Ici on importe
explicitement `tooling::something_or_nothing::SomethingOrNothing` et on met les fonctions à exécuter dans un main.
Pour que ce "bruit" n'apparaisse pas dans la documentation, on préfixe les lignes par des `#`. Ainsi ces lignes
sont lues par le compilateur pour les tests de documentation mais sont ignorées lors du rendu de la documentation.

Il y a également un exemple sur l'utilisation de la fonction `find_min()` (toujours dans `something_or_nothing.rs`)
```rust,ignore
{{#include ../../codes/rust_lang/tooling/src/something_or_nothing.rs:find_min}}
```

Finalement, les tests de documentation peuvent également être mis dans les documentation de module comme dans `minimum.rs`
```rust,ignore
{{#include ../../codes/rust_lang/tooling/src/minimum.rs:min}}
```

#### Rapport sur l'exécution des tests

Lors de l'appel à `cargo test` tous les tests sont exécutés et un rapport est généré. Sur
[ce code](#le-code), on obtient
```bash
$ cargo  est                                                                                               
   Compiling tooling v0.1.0 (/home/orestis/git/projects/rust-101/codes/rust_lang/tooling)
    Finished test [unoptimized + debuginfo] target(s) in 0.37s
     Running unittests src/lib.rs (target/debug/deps/tooling-f12750c4987ae624)

running 5 tests
test test_creation ... ok
test minimum::tests::test_min_i32 ... ok
test tests::test_min ... ok
test tests::test_failure_creation - should panic ... ok
test tests::test_min_something_or_nothing ... ok

test result: ok. 5 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s

     Running unittests src/main.rs (target/debug/deps/tooling-b63e62707c6aab7d)

running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s

   Doc-tests tooling

running 3 tests
test src/minimum.rs - minimum (line 8) ... ok
test src/something_or_nothing.rs - something_or_nothing::SomethingOrNothing<T> (line 38) ... ok
test src/something_or_nothing.rs - something_or_nothing::find_min (line 95) ... ok

test result: ok. 3 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.14s
```

### Les outils en plus du compilateur

La chaîne de compilation Rust vient avec deux autres outils très pratiques (en plus de `cargo` et `rustdoc`): `rustfmt` un formatteur de code, et `clippy` un linter.

#### `rustfmt`

Rust a fait le choix fasciste de définir des tas de conventions pour le nommage
des variables, des types, etc. L'outil `rustfmt` permet de formatter automatiquement
le code pour avoir un style uniforme au travers de tout votre code et ainsi améliorer
sa lisibilité.

```rust
fn main() {
    const size: usize = 9;
    let tab: [i32; size] = [10, 32, 12, 
    43, 52, 53, 83, 2, 9];
    
    if (size == 0)
    {
        panic!("Size is of tab = 0.");
    }

    println!("Among the numbers in the list:");
    for i in 0..size {
        print!("{} ", tab[i]);
    }
    println!();

    let mut MinTab=tab[0];
    for i in 1..size 
    {
        if MinTab > tab[i] 
        {
            MinTab=tab[i];
        }
    }



    println!("The minimal value is: {}", MinTab);
}
```

Afin de le voir à l'oeuvre copier le code ci-dessus dans un `src/main.rs` et utilisez la commande `rustfmt --check src/main.rs`
et observez le résultat. Par défaut `rustfmt` va modifier le code, ainsi l'option `--check` va uniquement montrer
quelles sont les choses à corriger. En général, `rustfmt` est intégré avec les plugins dans des éditeurs de code
tels que `codium` qui l'intègre dans le plugin `rust-analyzer` par exemple.

#### `clippy`

L'outil `clippy` est un détecteur automatique de mauvaise pratiques de codage, telles que définies
par la communauté du Rust. Il permet d'écrire du code le plus idiomatique possible
et en général d'éviter certaines mauvaises pratiques et/ou de simplifier
le code avec des *patterns* connus.

Comme ci-dessus, prenez ce code et exécutez la commande `cargo clippy` pour voir les recommandations du linter.

## Rustlings

Les rustlings à faire dans ce chapitre sont les suivants:

### Les modules

```bash
$ rustlings run tests1
$ rustlings run tests2
$ rustlings run tests3
$ rustlings run tests4
```

### Clippy

```bash
$ rustlings run clippy1
$ rustlings run clippy2
$ rustlings run clippy3
```
